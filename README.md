# API

This project is an REST API for all the information in the heritage project this information is going to be used in the [Android App](https://gitlab.com/camera_felgueiras/patrimonio/cm-app) and for the [BackOffice](https://gitlab.com/camera_felgueiras/patrimonio/cm-backoffice).

# Table of Contents

1. [Installation](#installation)
1. [Usage](#usage)
1. [Credits](#credits)
1. [License](#license)

# Installation <a name="installation"></a>

1. Clone the Repo using `git clone git@gitlab.com:camera_felgueiras/patrimonio/cm-api.git <FolderName>`
2. Cd into the repo folder `cd <FolderName>`
3. Install node dependencies `npm install`
4. Execute the `db.sh` file
   1. Mysql Username: `mysql username here`
   2. Mysql Password: `mysql password here`
   3. Clean Database [y/N]? `say yes and it will create the database for you`
   4. List all of the procedures executed

![image](https://i.imgur.com/mqieYDV.png)

5. Start the development mode `npm start`

The `.env` file contains the basic definitions like the app status like `dev` for development, the `PORT` witch the app is going to run on, the database configuration like the `DB_USER` witch is the username of the user responsible to the schema `DB_NAME` and _JSON WEB TOKEN_ configuration that may be used i still don't know. If there is no .env file the configurations will be on `config/config.js`, making all variables available on the app at any time.

```javascript
APP=dev
PORT=8080

DB_DIALECT=mysql
DB_HOST=localhost
DB_PORT=3306
DB_NAME=cm_pat
DB_USER=root
DB_PASSWORD=password

JWT_ENCRYPTION=58$n@#IKH$kG
JWT_EXPIRATION=10000
```

# Usage <a name="usage"></a>

The Routes Documentation can be found [here](https://documenter.getpostman.com/view/6260443/S1TZyFjz).

For routes that don't need authentication can be setup in `config/jwt.js` exemple:

```javascript
const path = [
  { url: /^\/api\/type\/*/, methods: ['GET'] },
]
```

To see a diagram of the database if you are using Mysql Workbench, you just need to reverse engineering the database
[tutorial here](https://dataedo.com/kb/tools/mysql-workbench/create-database-diagram), if not using
mysql workbench, you can try this [database diagram tools](https://dbmstools.com/categories/database-diagram-tools/mariadb)

# Credits <a name="credits"></a>

- Main Developer @Zyr0

# License <a name="License"></a>
