const CONFIG = require('./config');
const expressJwt = require('express-jwt');

const path = [
  { url: /^\/api\/user/, methods: ['POST'] },
  { url: /^\/api\/user\/auth/, methods: ['POST'] },
  { url: /^\/api\/image\/*/, methods: ['GET'] },
  { url: /^\/api\/type\/*/, methods: ['GET'] },
  { url: /^\/api\/category\/*/, methods: ['GET'] },
  { url: /^\/api\/personality\/*/, methods: ['GET'] },
  { url: /^\/api\/local\/*/, methods: ['GET'] },
  { url: /^\/api\/route\/*/, methods: ['GET'] },
  { url: /^\/api\/timeline\/*/, methods: ['GET'] },
];

function jwt() {
  const secret = CONFIG.jwt_encryption;
  return expressJwt({ secret, credentialsRequired: true }).unless({ path });
}

module.exports = jwt;
