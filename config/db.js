const mysql = require('mysql');
const util = require('util');
const CONFIG = require('./config');

const connection = mysql.createPool({
  host: CONFIG.db_host,
  port: CONFIG.db_port,
  user: CONFIG.db_user,
  password: CONFIG.db_password,
  database: CONFIG.db_name
});

module.exports = connection;
