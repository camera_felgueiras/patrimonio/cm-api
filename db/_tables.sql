DROP DATABASE IF EXISTS cm_pat;
CREATE DATABASE IF NOT EXISTS cm_pat;
USE cm_pat;

-- DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
 `id` INT PRIMARY KEY AUTO_INCREMENT,
 `email` VARCHAR(255) NOT NULL,
 `passwd` VARCHAR(100) NOT NULL,
 `first_name` VARCHAR(100) NOT NULL,
 `last_name` VARCHAR(100) NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `contact_info`;
CREATE TABLE IF NOT EXISTS `contact_info` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `tel` VARCHAR(9) NULL,
  `web` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  `address` VARCHAR(255) NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `path` VARCHAR(255) NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `id_type` INT NOT NULL,
  FOREIGN KEY (id_type) REFERENCES `type`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `personality`;
CREATE TABLE IF NOT EXISTS `personality` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `job` VARCHAR(255) NOT NULL,
  `birth` VARCHAR(255) NOT NULL,
  `death` VARCHAR(255) NOT NULL,
  `desc` MEDIUMTEXT NOT NULL,
  `profile_pic_id` INT NOT NULL,
  FOREIGN KEY (profile_pic_id) REFERENCES `image`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `p_image`;
CREATE TABLE IF NOT EXISTS `p_image` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_personality` INT NOT NULL,
  `id_image` INT NOT NULL,
  FOREIGN KEY (id_personality) REFERENCES `personality`(id),
  FOREIGN KEY (id_image) REFERENCES `image`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `local`;
CREATE TABLE IF NOT EXISTS `local` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `likes` INT NOT NULL,
  `lat` DOUBLE NOT NULL,
  `lng` DOUBLE NOT NULL,
  `desc` TEXT NOT NULL,
  `id_contact_info` INT NOT NULL,
  `id_main_image` INT NOT NULL,
  FOREIGN KEY (id_contact_info) REFERENCES `contact_info`(id),
  FOREIGN KEY (id_main_image) REFERENCES `image`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `l_category`;
CREATE TABLE IF NOT EXISTS `l_category` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_category` INT NOT NULL,
  `id_local` INT NOT NULL,
  FOREIGN KEY (id_category) REFERENCES `category`(id),
  FOREIGN KEY (id_local) REFERENCES `local`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `l_images`;
CREATE TABLE IF NOT EXISTS `l_images` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_local` INT NOT NULL,
  `id_image` INT NOT NULL,
  FOREIGN KEY (id_local) REFERENCES `local`(id),
  FOREIGN KEY (id_image) REFERENCES `image`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `route`;
CREATE TABLE IF NOT EXISTS `route` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `likes` VARCHAR(255) NOT NULL,
  `km` VARCHAR(255) NOT NULL,
  `website` VARCHAR(255) NOT NULL,
  `desc` TEXT NOT NULL,
  `id_main_image` INT NOT NULL,
  FOREIGN KEY (id_main_image) REFERENCES `image`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `path`;
CREATE TABLE IF NOT EXISTS `path` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_local` INT NOT NULL,
  `id_route` INT NOT NULL,
  FOREIGN KEY (id_local) REFERENCES `local`(id),
  FOREIGN KEY (id_route) REFERENCES `route`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `r_category`;
CREATE TABLE IF NOT EXISTS `r_category` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_category` INT NOT NULL,
  `id_route` INT NOT NULL,
  FOREIGN KEY (id_category) REFERENCES `category`(id),
  FOREIGN KEY (id_route) REFERENCES `route`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `r_image`;
CREATE TABLE IF NOT EXISTS `r_image` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_route` INT NOT NULL,
  `id_image` INT NOT NULL,
  FOREIGN KEY (id_route) REFERENCES `route`(id),
  FOREIGN KEY (id_image) REFERENCES `image`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `p_has_l`;
CREATE TABLE IF NOT EXISTS `p_has_l` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_personality` INT NOT NULL,
  `id_local` INT NOT NULL,
  FOREIGN KEY (id_personality) REFERENCES `personality`(id),
  FOREIGN KEY (id_local) REFERENCES `local`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `p_has_r`;
CREATE TABLE IF NOT EXISTS `p_has_r` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_personality` INT NOT NULL,
  `id_route` INT NOT NULL,
  FOREIGN KEY (id_personality) REFERENCES `personality`(id),
  FOREIGN KEY (id_route) REFERENCES `route`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `l_has_r`;
CREATE TABLE IF NOT EXISTS `l_has_r` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_local` INT NOT NULL,
  `id_route` INT NOT NULL,
  FOREIGN KEY (id_local) REFERENCES `local`(id),
  FOREIGN KEY (id_route) REFERENCES `route`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `l_has_l`;
CREATE TABLE IF NOT EXISTS `l_has_l` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_local` INT NOT NULL,
  `id_l` INT NOT NULL,
  FOREIGN KEY (id_local) REFERENCES `local`(id),
  FOREIGN KEY (id_l) REFERENCES `local`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `p_has_p`;
CREATE TABLE IF NOT EXISTS `p_has_p` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_personality` INT NOT NULL,
  `id_p` INT NOT NULL,
  FOREIGN KEY (id_personality) REFERENCES `personality`(id),
  FOREIGN KEY (id_p) REFERENCES `personality`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `r_has_r`;
CREATE TABLE IF NOT EXISTS `r_has_r` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_route` INT NOT NULL,
  `id_r` INT NOT NULL,
  FOREIGN KEY (id_route) REFERENCES `route`(id),
  FOREIGN KEY (id_r) REFERENCES `route`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `timeline`;
CREATE TABLE IF NOT EXISTS `timeline` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `date` VARCHAR(255) NOT NULL,
  `desc` TEXT NOT NULL,
  `id_image` INT NOT NULL,
  FOREIGN KEY (`id_image`) REFERENCES `image`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `timeline_p`;
CREATE TABLE IF NOT EXISTS `timeline_p` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_p` INT NOT NULL,
  `id_t` INT NOT NULL,
  FOREIGN KEY (id_p) REFERENCES `personality`(id),
  FOREIGN KEY (id_t) REFERENCES `timeline`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

-- DROP TABLE IF EXISTS `timeline_l`;
CREATE TABLE IF NOT EXISTS `timeline_l` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `id_l` INT NOT NULL,
  `id_t` INT NOT NULL,
  FOREIGN KEY (id_l) REFERENCES `local`(id),
  FOREIGN KEY (id_t) REFERENCES `timeline`(id)
) ENGINE=InnoDB CHARACTER SET=latin1 DEFAULT COLLATE latin1_bin;

INSERT INTO `type`(`name`) VALUES('Património Histórico'),('Natureza'),('Rota');
