-- GET ALL TYPES
DELIMITER //
DROP PROCEDURE IF EXISTS `types`;
CREATE PROCEDURE `types` ()
BEGIN 
  SELECT * FROM `type`;
END // 

-- GET TYPE BY ID
DROP PROCEDURE IF EXISTS `get_type`;
CREATE PROCEDURE `get_type` (
  IN p_id INT,
  OUT result VARCHAR(1) 
)
BEGIN
  DECLARE b_id_type INT;

  SELECT EXISTS (
    SELECT `id` FROM `type` WHERE `id` = p_id 
  ) INTO b_id_type;

  SET result = 0;
  IF (b_id_type = 1) THEN
    SELECT * FROM `type` WHERE `id` = p_id; 
  END IF;
  SELECT result;
END // 

-- CREATE NEW TYPE
DROP PROCEDURE IF EXISTS `add_type`;
CREATE PROCEDURE `add_type` (
  IN p_name VARCHAR(255),
  OUT result VARCHAR(1)
)
BEGIN 
  DECLARE b_id_type INT;

  SELECT EXISTS (
    SELECT `id` FROM `type` WHERE `name` = p_name
  ) INTO b_id_type;

  SET result = 0;
  IF (b_id_type = 0) THEN 
    INSERT INTO `type`(`name`) VALUES(p_name);
    SET result = 1;
  END IF;
  SELECT result;
END // 

-- DELETE TYPE
DROP PROCEDURE IF EXISTS `delete_type`;
CREATE PROCEDURE `delete_type` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_id_type INT;

  SELECT EXISTS (
    SELECT `id` FROM `type` WHERE `id` = p_id 
  ) INTO b_id_type;

  SET result = 0;
  IF (b_id_type = 1) THEN
    DELETE FROM `type` WHERE `id` = p_id;
    SET result = 1;
  END IF;
  SELECT result;
END //
DELIMITER ;