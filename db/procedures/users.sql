-- SELECT ALL USERS
DELIMITER //
DROP PROCEDURE IF EXISTS users;
CREATE PROCEDURE users()
BEGIN
  SELECT * FROM `user`;
END //

-- INSERT NEW USER
DROP PROCEDURE IF EXISTS create_user;
CREATE PROCEDURE create_user(
  IN p_email VARCHAR(255),
  IN p_passwd VARCHAR(100),
  IN p_first_name VARCHAR(100),
  IN p_last_name VARCHAR(100),
  OUT result VARCHAR(250)
)
BEGIN
  DECLARE b_id_user INT;

  SELECT EXISTS (
    SELECT id FROM user WHERE email = p_email
  ) INTO b_id_user;

  IF (b_id_user = 0) THEN
    INSERT INTO user(email, passwd, first_name, last_name) VALUES(p_email, p_passwd, p_first_name, p_last_name);
    SET result = 1;
  ELSE
    SET result = 0;
  END IF;

  SELECT result;
END //

-- AUTH USER
DROP PROCEDURE IF EXISTS auth;
CREATE PROCEDURE auth(
  IN p_email VARCHAR(255),
  OUT result VARCHAR(250)
)
BEGIN
  DECLARE b_id_user INT;

  SELECT EXISTS (
    SELECT id FROM user WHERE email = p_email
  ) INTO b_id_user;

  -- IF EXISTS RETURN 0
  IF (b_id_user = 1) THEN
    SELECT * FROM user WHERE email = p_email;
  ELSE
    SET result = 0;
    SELECT result;
  END IF;
END //

-- GET USER
DROP PROCEDURE IF EXISTS get_user;
CREATE PROCEDURE get_user(
  IN p_id INT,
  OUT result VARCHAR(200)
)
BEGIN
  DECLARE b_id_user INT;

  SELECT EXISTS (
    SELECT id FROM user WHERE id = p_id
  ) INTO b_id_user;

  IF (b_id_user = 1) THEN
    SELECT * FROM user WHERE id = p_id;
  ELSE
    SET result = 0;
    SELECT result;
  END IF;
END //

-- DELETE USER
DROP PROCEDURE IF EXISTS delete_user;
CREATE PROCEDURE delete_user(
  IN p_id INT,
  OUT result VARCHAR(200)
)
BEGIN
  DECLARE b_id_user INT;

  SELECT EXISTS (
    SELECT id FROM user WHERE id = p_id
  ) INTO b_id_user;

  -- RETURN 1 IF DELETED
  IF (b_id_user = 1) THEN
    DELETE FROM user WHERE id = p_id;
    SET result = 1;
  ELSE
    SET result = 0;
  END IF;
  SELECT result;
END //

-- UPDATE USER
DROP PROCEDURE IF EXISTS update_user;
CREATE PROCEDURE update_user(
  IN p_id INT,
  IN p_email VARCHAR(255),
  IN p_first_name VARCHAR(100),
  IN p_last_name VARCHAR(100),
  OUT result VARCHAR(250)
)
BEGIN
  DECLARE b_email_id INT;
  DECLARE b_email INT;

  -- SE IF EMAIL BELONGS TO USER
  SELECT EXISTS (
    SELECT email FROM user WHERE id = p_id AND email = p_email
  ) INTO b_email_id;

  -- SE IF EMAIL EXISTS
  SELECT EXISTS (
    SELECT email FROM user WHERE email = p_email
  ) INTO b_email;

  SET result = 0;
  IF (b_email_id = 1) THEN
    UPDATE user SET email = p_email,
      first_name = p_first_name,
      last_name = p_last_name
    WHERE id = p_id;
    SELECT * FROM user WHERE id = p_id;
  ELSEIF (b_email = 0) THEN
    UPDATE user SET email = p_email,
      first_name = p_first_name,
      last_name = p_last_name
    WHERE id = p_id;
    SELECT * FROM user WHERE id = p_id;
  END IF;
  SELECT result;
END //

-- UPDATE PASSWORD 
DROP PROCEDURE IF EXISTS update_passwd;
CREATE PROCEDURE update_passwd(
  IN p_id INT,
  IN p_password VARCHAR(255),
  OUT result VARCHAR(255)
)
BEGIN
  DECLARE b_id INT;

  SELECT EXISTS (
    SELECT id FROM user WHERE id = p_id
  ) INTO b_id;

  IF (b_id = 1) THEN
    UPDATE user SET passwd = p_password WHERE id = p_id;
    SET result = 1;
  ELSE
    SET result = 0;
  END IF;
  SELECT result;
END //
DELIMITER ;
