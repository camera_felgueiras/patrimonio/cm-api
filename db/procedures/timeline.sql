DELIMITER //
-- GET ALL FROM TIMELINE
DROP PROCEDURE IF EXISTS `all_timeline`;
CREATE PROCEDURE `all_timeline` ()
BEGIN
  CALL `all_timeline_p`;
  CALL `all_timeline_l`;
END //

-- GET ALL TIMELINE RELATED WITH PERSONALITIES;
DROP PROCEDURE IF EXISTS `all_timeline_p`;
CREATE PROCEDURE `all_timeline_p` ()
BEGIN
  SELECT t.*, i.path, p.name as `related_name`, ip.path as `related_image` FROM `timeline_p` tp
    INNER JOIN `timeline` t ON t.id = tp.id_t
    INNER JOIN `image` i ON i.id = t.id_image
    INNER JOIN `personality` p ON p.id = tp.id_p
    INNER JOIN `image` ip ON ip.id = p.profile_pic_id;
END //

-- GET ALL TIMELINE RELATED WITH LOCALS;
DROP PROCEDURE IF EXISTS `all_timeline_l`;
CREATE PROCEDURE `all_timeline_l` ()
BEGIN
  SELECT t.*, i.path, l.title as `related_name`, ip.path as `related_image` FROM `timeline_l` tl
    INNER JOIN `timeline` t ON t.id = tl.id_t
    INNER JOIN `image` i ON i.id = t.id_image
    INNER JOIN `local` l ON l.id = tl.id_l
    INNER JOIN `image` ip ON ip.id = l.id_main_image;
END //

-- RELATE TIMELINE WITH PERSONALITY LOCAL
DROP PROCEDURE IF EXISTS `relate_timeline`;
CREATE PROCEDURE `relate_timeline` (
  IN p_title VARCHAR(255),
  IN p_date VARCHAR(255),
  IN p_desc TEXT,
  IN p_path VARCHAR(255),
  IN p_type VARCHAR(255),
  IN p_id_conn INT,
  OUT result VARCHAR(1)
)
BEGIN 
  DECLARE v_t_id INT;
  DECLARE v_image_id INT;

  SELECT `id` INTO v_image_id FROM `image` WHERE `path` = p_path;

  INSERT INTO `timeline`(`title`, `date`, `desc`, `id_image`) VALUES(p_title, p_date, p_desc, v_image_id);
  SELECT MAX(`id`) INTO v_t_id FROM `timeline` WHERE `title` = p_title AND `date` = p_date AND `desc` = p_desc;

  IF (p_type LIKE 'p') THEN
    INSERT INTO `timeline_p`(`id_p`, `id_t`) VALUES(p_id_conn, v_t_id);
  ELSE
    INSERT INTO `timeline_l`(`id_l`, `id_t`) VALUES(p_id_conn, v_t_id);
  END IF;
END //

-- GET TIMELINE BY ID
DROP PROCEDURE IF EXISTS `get_timeline`;
CREATE PROCEDURE `get_timeline` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_t_id INT;
  DECLARE b_tp_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `timeline` WHERE `id` = p_id
  ) INTO b_t_id;

  SET result = 0;
  IF (b_t_id = 1) THEN
    SELECT EXISTS (
      SELECT `id` FROM `timeline_p` WHERE id_t = p_id
    ) INTO b_tp_id;

    SET result = 1;

    IF (b_tp_id = 1) THEN
      SELECT tp.id as `id_t_related`, t.*, i.path, p.name as `related_name`, ip.path as `related_image` FROM `timeline_p` tp
        INNER JOIN `timeline` t ON t.id = tp.id_t
        INNER JOIN `image` i ON i.id = t.id_image
        INNER JOIN `personality` p ON p.id = tp.id_p
        INNER JOIN `image` ip ON ip.id = p.profile_pic_id
      WHERE t.id = p_id;
    ELSE
      SELECT tl.id as `id_t_related`, t.*, i.path, l.title as `related_name`, ip.path as `related_image` FROM `timeline_l` tl
        INNER JOIN `timeline` t ON t.id = tl.id_t
        INNER JOIN `image` i ON i.id = t.id_image
        INNER JOIN `local` l ON l.id = tl.id_l
        INNER JOIN `image` ip ON ip.id = l.id_main_image
      WHERE t.id = p_id;
    END IF;
  END IF;
  SELECT result;
END //

-- DELETE TIMELINE
DROP PROCEDURE IF EXISTS `delete_timeline`;
CREATE PROCEDURE `delete_timeline` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_t_id INT;
  DECLARE b_tp_id INT;
  DECLARE image_path VARCHAR(255);

  SELECT EXISTS (
    SELECT `id` FROM `timeline` WHERE id = p_id
  ) INTO b_t_id;

  SET result = 0;
  IF (b_t_id = 1) THEN
    SELECT i.path INTO image_path FROM `timeline` t
      INNER JOIN `image` i ON i.id = t.id_image 
      WHERE t.id = p_id;

    SET result = 1;
    SELECT EXISTS (
      SELECT `id` FROM `timeline_p` WHERE `id_t` = p_id
    ) INTO b_tp_id;

    IF (b_tp_id = 1) THEN 
      DELETE FROM `timeline_p` WHERE `id_t` = p_id; 
    ELSE
      DELETE FROM `timeline_l` WHERE `id_t` = p_id;
    END IF;
    DELETE FROM `timeline` WHERE `id` = p_id;
    DELETE FROM `image` WHERE `path` = image_path; 
  END IF;
  SELECT result, image_path;
END //

-- UPDATE TIMELINE
DROP PROCEDURE IF EXISTS `update_timeline`;
CREATE PROCEDURE `update_timeline` (
  IN p_id INT,
  IN p_id_t INT,
  IN p_title VARCHAR(255),
  IN p_date VARCHAR(255),
  IN p_desc TEXT,
  IN p_image VARCHAR(255),
  IN p_type VARCHAR(255),
  IN p_id_conn INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_timeline INT;
  DECLARE v_image_id INT;
  DECLARE image_path VARCHAR(255);

  SELECT `id` INTO v_image_id FROM `image` WHERE `path` = p_image;

  SELECT EXISTS (
    SELECT `id` FROM `timeline` WHERE `id` = p_id
  ) INTO b_timeline;

  SET result = 0;
  IF (b_timeline = 1) THEN
    SELECT i.path INTO image_path FROM `timeline` t
      INNER JOIN `image` i ON i.id = t.id_image 
      WHERE t.id = p_id;

    UPDATE `timeline` SET `title` = p_title, `date` = p_date, `desc` = p_desc, `id_image` = v_image_id WHERE `id` = p_id;

    DELETE FROM `timeline_p` WHERE `id` = p_id_t AND `id_t` = p_id;
    DELETE FROM `timeline_l` WHERE `id` = p_id_t AND `id_t` = p_id;
    IF (p_type LIKE 'p') THEN
      INSERT INTO `timeline_p`(`id_p`, `id_t`) VALUES(p_id_conn, p_id);
    ELSE
      INSERT INTO `timeline_l`(`id_l`, `id_t`) VALUES(p_id_conn, p_id);
    END IF;
    SET result = 1;
  END IF;
  SELECT result, image_path;
END //

-- GET TIMELINE RELATED
DROP PROCEDURE IF EXISTS `get_timeline_related`;
CREATE PROCEDURE `get_timeline_related` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_t INT;
  DECLARE b_tp_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `timeline` WHERE `id` = p_id
  ) INTO b_t;

  SET result = 0;
  IF (b_t = 1) THEN 
    SELECT EXISTS (
      SELECT `id` FROM `timeline_p` WHERE `id_t` = p_id
    ) INTO b_tp_id;

    SET result = 1;

    IF (b_tp_id = 1) THEN
      SELECT p.*, i.path FROM `timeline_p` tp
        INNER JOIN `timeline` t ON t.id = tp.id_t
        INNER JOIN `personality` p ON p.id = tp.id_p
        INNER JOIN `image` i ON i.id = p.profile_pic_id
      WHERE t.id = p_id;
    ELSE
      SELECT DISTINCT l.*, i.path, t.name as `type` FROM `timeline_l` tl
        INNER JOIN `timeline` ti ON ti.id = tl.id_t
        INNER JOIN `local` l ON l.id = tl.id_l
        INNER JOIN `image` i ON i.id = l.id_main_image 
        INNER JOIN `l_category` lc ON lc.id_local = l.id
        INNER JOIN `category` c ON c.id = lc.id_category
        INNER JOIN `type` t ON t.id = c.id_type
      WHERE ti.id = p_id;
    END IF;
  END IF;
  SELECT result;
END //

-- GET TIMELINE IMAGE
DROP PROCEDURE IF EXISTS `get_timeline_image`;
CREATE PROCEDURE `get_timeline_image` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_t INT;

  SELECT EXISTS (
    SELECT `id` FROM `timeline` WHERE `id` = p_id
  ) INTO b_t;

  SET result = 0;
  IF (b_t = 1) THEN 
    SELECT i.path as `name`, i.path as `url` FROM `image` i 
      INNER JOIN `timeline` t ON t.id_image = i.id
    WHERE t.id = p_id;
    SET result = 1;
  END IF;
  SELECT result;
END //
DELIMITER ;
