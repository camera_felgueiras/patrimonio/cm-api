-- ALL PERSONALITIES
DELIMITER //
DROP PROCEDURE IF EXISTS `personalities`;
CREATE PROCEDURE `personalities` ()
BEGIN
  SELECT p.*, id_i.path FROM `personality` p
    INNER JOIN `image` id_i ON id_i.id = p.profile_pic_id;
END //

-- GET PERSONALITY
DROP PROCEDURE IF EXISTS `get_personality`;
CREATE PROCEDURE `get_personality` (
  IN p_id INT,
  OUT result VARCHAR(255)
)
BEGIN
  DECLARE b_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `personality` WHERE `id` = p_id
  ) INTO b_id;

  IF (b_id = 1) THEN
    SELECT p.*, id_i.path FROM `personality` p
      INNER JOIN `image` id_i ON id_i.id = p.profile_pic_id
      WHERE p.id = p_id;
  ELSE
    SET result = 0;
    SELECT result;
  END IF;
END //

-- GET PERSONALITY _ HAS _ PERSONALITY
DROP PROCEDURE IF EXISTS `get_p_has_p`;
CREATE PROCEDURE `get_p_has_p` (
  IN p_id INT
)
BEGIN
  SELECT pp.id_p as related_id, p.name as related_name, i.path as related_image FROM `p_has_p` pp
    INNER JOIN `personality` p ON p.id = pp.id_p
    INNER JOIN `image` i ON i.id = p.profile_pic_id
    WHERE pp.id_personality = p_id;
END //

-- GET PERSONALITY _ HAS _ LOCAL
DROP PROCEDURE IF EXISTS `get_p_has_l`;
CREATE PROCEDURE `get_p_has_l` (
  IN p_id INT
)
BEGIN
  SELECT pl.id_local as related_id, l.title as related_name, i.path as related_image FROM `p_has_l` pl
    INNER JOIN `local` l ON l.id = pl.id_local
    INNER JOIN `image` i ON i.id = l.id_main_image
    WHERE pl.id_personality = p_id;
END //

-- GET PERSONALITY_IMAGES
DROP PROCEDURE IF EXISTS `get_personality_image`;
CREATE PROCEDURE `get_personality_image` (
  IN p_personality_id INT
)
BEGIN
  SELECT i.id, i.path, i.path as `name` FROM `image` i INNER JOIN `personality` p ON p.profile_pic_id = i.id WHERE p.id = p_personality_id;
  SELECT i.id, i.path, i.path as `name` FROM `image` i INNER JOIN `p_image` pi ON pi.id_image = i.id WHERE pi.id_personality = p_personality_id;
END //

-- CREATE PERSONALTY
DROP PROCEDURE IF EXISTS `add_personality`;
CREATE PROCEDURE `add_personality` (
  IN p_name VARCHAR(255),
  IN p_job VARCHAR(255),
  IN p_birth VARCHAR(255),
  IN p_death VARCHAR(255),
  IN p_desc MEDIUMTEXT,
  IN p_profile_pic_path VARCHAR(255),
  OUT result VARCHAR(255)
)
BEGIN
  DECLARE b_id INT;
  DECLARE v_profile_pic_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `personality` WHERE `name` = p_name AND `job` = p_job AND `birth` = p_birth AND `death` = p_death
  ) INTO b_id;

  SET result = 0;
  IF (b_id = 0) THEN
    SELECT `id` INTO v_profile_pic_id FROM `image` WHERE `path` = p_profile_pic_path;
    INSERT INTO `personality`(`name`, `job`, `birth`, `death`, `desc`, `profile_pic_id`) VALUES(p_name, p_job, p_birth, p_death, p_desc, v_profile_pic_id);
    SELECT * FROM `personality` ORDER BY `id` DESC LIMIT 1;
  END IF;
  SELECT result;
END //

-- ADD RELATION
DROP PROCEDURE IF EXISTS `add_p_relation`;
CREATE PROCEDURE `add_p_relation` (
  IN id_p INT,
  IN id_to_add INT,
  IN p_type VARCHAR(255)
)
BEGIN
  IF (p_type = 'route') THEN
    INSERT INTO `p_has_r`(`id_personality`, `id_rota`) VALUES(id_p, id_to_add);
  ELSEIF (p_type = 'personality') THEN
    INSERT INTO `p_has_p`(`id_personality`, `id_p`) VALUES(id_p, id_to_add);
    INSERT INTO `p_has_p`(`id_personality`, `id_p`) VALUES(id_to_add, id_p);
  ELSE
    INSERT INTO `p_has_l`(`id_personality`, `id_local`) VALUES(id_p, id_to_add);
  END IF;
END //

-- ADD IMAGE PERSONALITY
DROP PROCEDURE IF EXISTS `personality_image`;
CREATE PROCEDURE `personality_image` (
  IN p_personality_id INT,
  IN p_path VARCHAR(255)
)
BEGIN
  DECLARE b_i INT;
  DECLARE image_id INT;

  SELECT `id` INTO image_id FROM `image` WHERE `path` = p_path;
  SELECT EXISTS (
    SELECT `id` FROM `p_image` WHERE `id_image` = image_id
  ) INTO b_i;

  IF (b_i = 0) THEN
    INSERT INTO `p_image`(`id_personality`, `id_image`) VALUES(p_personality_id, image_id);
  END IF;
END //

-- GET TIMELINE PERSONALITY
DROP PROCEDURE IF EXISTS `get_timeline_p`;
CREATE PROCEDURE `get_timeline_p` (
  IN p_id INT
)
BEGIN
  SELECT t.*, i.path as `image` FROM `timeline` t
    INNER JOIN `timeline_p` tp ON tp.id_t = t.id
    INNER JOIN `image` i ON i.id = t.id_image
    WHERE tp.id_p = p_id;
END //

-- DELETE PERSONALITY IMAGES
DROP PROCEDURE IF EXISTS `delete_p_image`;
CREATE PROCEDURE `delete_p_image` (
  IN p_path VARCHAR(255)
)
BEGIN
  DECLARE img_id INT;
  SELECT `id` INTO img_id FROM `image` WHERE `path` = p_path;
  DELETE FROM `p_image` WHERE `id_image` = img_id;
  DELETE FROM `image` WHERE `id` = img_id;
END //

-- DELETE PERSONALITY TIMELINE
DROP PROCEDURE IF EXISTS `delete_timeline_p`;
CREATE PROCEDURE `delete_timeline_p`(
  IN p_id VARCHAR(255)
)
BEGIN
  DECLARE v_image INT;
  DELETE FROM `timeline_p` WHERE `id_t` = p_id;
  SELECT `id_image` INTO v_image FROM `timeline` WHERE `id` = p_id;
  DELETE FROM `timeline` WHERE `id` = p_id;
  DELETE FROM `image` WHERE `id` = v_image;
END //

-- DELETE PERSONALITY
DROP PROCEDURE IF EXISTS `delete_personality`;
CREATE PROCEDURE `delete_personality` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_id INT;
  DECLARE timeline_id INT;
  DECLARE image_id INT;
  DECLARE profile_pic INT;
  DECLARE pic VARCHAR(255);
  DECLARE n INT DEFAULT 0;
  DECLARE i INT DEFAULT 0;

  SELECT EXISTS (
    SELECT `id` FROM `personality` WHERE `id` = p_id
  ) INTO b_id;

  SET result = 0;
  IF (b_id = 1) THEN
    CALL delete_personality_related(p_id);

    SELECT p.profile_pic_id INTO profile_pic FROM `personality` p
      INNER JOIN `image` img ON img.id = p.profile_pic_id
      WHERE p.id = p_id;
    DELETE FROM `personality` WHERE `id` = p_id;

    SELECT `path` INTO pic FROM `image` WHERE `id` = profile_pic;
    DELETE FROM `image` WHERE `id` = profile_pic;
    SELECT pic;
  ELSE
    SELECT result;
  END IF;
END //

-- EXISTS
DROP PROCEDURE IF EXISTS `p_exists`;
CREATE PROCEDURE `p_exists`(
  IN p_id INT
)
BEGIN
  DECLARE result INT;

  SELECT EXISTS (
    SELECT * FROM `personality` WHERE `id` = p_id
  ) INTO result;
  SELECT result;
END //

-- GET RELATED PERSONALITY
DROP PROCEDURE IF EXISTS `get_related_personality`;
CREATE PROCEDURE `get_related_personality` (
  IN p_id INT
)
BEGIN
  SELECT p.*, i.path FROM `p_has_p` pp
    INNER JOIN `personality` p ON p.id = pp.id_personality
    INNER JOIN `image` i ON i.id = p.profile_pic_id
    WHERE pp.id_personality = p_id;
  SELECT l.*, i.path, t.name as `type` FROM `p_has_l` pl
    INNER JOIN `local` l ON l.id = pl.id_local
    INNER JOIN `image` i ON i.id = l.id_main_image
    INNER JOIN `l_category` lc ON lc.id_local = l.id
    INNER JOIN `category` c ON c.id = lc.id_category
    INNER JOIN `type` t ON t.id = c.id_type
    WHERE pl.id_personality = p_id;
  SELECT r.*, i.path, t.name as `type` FROM `p_has_r` pr
    INNER JOIN `route` r ON r.id = pr.id_route
    INNER JOIN `image` i ON i.id = r.id_main_image
    INNER JOIN `r_category` rc ON rc.id_route = r.id
    INNER JOIN `category` c ON c.id = rc.id_category
    INNER JOIN `type` t ON t.id = c.id_type
    WHERE pr.id_personality = p_id;
END //

-- UPDATE PERSONALITY
DROP PROCEDURE IF EXISTS `update_personality`;
CREATE PROCEDURE `update_personality` (
  IN p_id INT,
  IN p_name VARCHAR(255),
  IN p_job VARCHAR(255),
  IN p_birth VARCHAR(255),
  IN p_death VARCHAR(255),
  IN p_desc MEDIUMTEXT,
  IN p_profile_pic_path VARCHAR(255),
  OUT result VARCHAR(255)
)
BEGIN
  DECLARE b_p INT;
  DECLARE v_image INT;
  DECLARE image_path VARCHAR(255);

  SELECT EXISTS (
    SELECT `id` FROM `personality` WHERE `id` = p_id
  ) INTO b_p;

  SET result = 0;
  IF (b_p = 1) THEN
    SELECT i.path INTO image_path FROM `image` i
      INNER JOIN `personality` p ON p.profile_pic_id = i.id
    WHERE p.id = p_id;
    SELECT `id` INTO v_image FROM `image` WHERE `path` = p_profile_pic_path;
    UPDATE `personality` SET `name` = p_name, `job` = p_job, `birth` = p_birth, `death` = p_death, `desc` = p_desc, profile_pic_id = v_image
      WHERE `id` = p_id;
    SET result = 1;
  END IF;
  SELECT result;
  SELECT image_path;
END //

-- DELETE RELATED PERSONALITY
DROP PROCEDURE IF EXISTS `delete_personality_related`;
CREATE PROCEDURE `delete_personality_related` (
  IN p_id INT
)
BEGIN
  DELETE FROM `p_has_p` WHERE `id_personality` = p_id;
  DELETE FROM `p_has_p` WHERE `id_p` = p_id;
  DELETE FROM `p_has_l` WHERE `id_personality` = p_id;
  DELETE FROM `p_has_r` WHERE `id_personality` = p_id;
END //
DELIMITER ;
