-- CREATE CONTACT_INFO
DELIMITER //
DROP PROCEDURE IF EXISTS `create_contact_info`;
CREATE PROCEDURE `create_contact_info`(
  IN p_tel VARCHAR(9),
  IN p_web VARCHAR(255),
  IN p_email VARCHAR(255),
  IN p_address VARCHAR(255)
) 
BEGIN 
  INSERT INTO `contact_info`(`tel`, `web`, `email`, `address`) VALUES(p_tel, p_web, p_email, p_address);
  SELECT `id` FROM `contact_info` ORDER BY `id` DESC LIMIT 1;
END //

-- UPDATE LOCAL CONTACT
DROP PROCEDURE IF EXISTS `update_local_contact`;
CREATE PROCEDURE `update_local_contact` (
  IN p_id_local INT,
  IN p_tel VARCHAR(9),
  IN p_web VARCHAR(255),
  IN p_email VARCHAR(255),
  IN p_address VARCHAR(255)
)
BEGIN
  DECLARE v_contact_id INT;

  SELECT `id_contact_info` INTO v_contact_id FROM `local` WHERE `id` = p_id_local;
  
  UPDATE `contact_info` SET `tel` = p_tel, `web` = p_web, `email` = p_email, `address` = p_address WHERE `id` = v_contact_id;
END //
DELIMITER ;
