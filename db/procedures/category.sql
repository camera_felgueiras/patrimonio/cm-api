-- GET ALL CATEGORIES
DELIMITER //
DROP PROCEDURE IF EXISTS `categories`;
CREATE PROCEDURE `categories` ()
BEGIN
  SELECT * FROM `category`;
END //

-- GET CATEGORY BY ID
DROP PROCEDURE IF EXISTS `get_category`;
CREATE PROCEDURE `get_category` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_id INT;

  SELECT EXISTS (
    SELECT * FROM `category` WHERE `id` = p_id
  ) INTO b_id;

  SET result = 0;
  IF (b_id = 1) THEN
    SELECT * FROM `category` WHERE `id` = p_id;
  END IF;
  SELECT result;
END //

-- GET CATEGORIES BY TYPE 
DROP PROCEDURE IF EXISTS `get_categories_type`;
CREATE PROCEDURE `get_categories_type` (
  IN p_id_type VARCHAR(255),
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `category` WHERE `id_type` = p_id_type
  ) INTO b_id;

  SET result = 0;
  IF (b_id = 1) THEN
    SELECT * FROM `category` WHERE `id_type` = p_id_type;
  END IF;
  SELECT result;
END //

-- ADD NEW CATEGORY
DROP PROCEDURE IF EXISTS `add_category`;
CREATE PROCEDURE `add_category` (
  IN p_name VARCHAR(255),
  IN p_id_type VARCHAR(255),
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_id INT;
  DECLARE b_type_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `type` WHERE `id` = p_id_type
  ) INTO b_type_id;

  SET result = 0;
  IF (b_type_id = 1) THEN
    SELECT EXISTS (
      SELECT `id` FROM `category` WHERE `name` = p_name AND `id_type` = p_id_type
    ) INTO b_id;

    SET result = 1;
    IF (b_id = 0) THEN
      INSERT INTO `category`(`name`, `id_type`) VALUES (p_name, p_id_type);
      SET result = 2;
    END IF;
  END IF;
  SELECT result;
END //

-- DELETE CATEGORY
DROP PROCEDURE IF EXISTS `delete_category`;
CREATE PROCEDURE `delete_category` (
  IN p_id INT,
  OUT result VARCHAR(255)
)
BEGIN
  DECLARE b_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `category` WHERE `id` = p_id
  ) INTO b_id;

  SET result = 0;
  IF (b_id = 1) THEN
    DELETE FROM `category` WHERE `id` = p_id;
    SET result = 1;
  END IF;
  SELECT result;
END //
DELIMITER ;