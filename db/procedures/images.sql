-- GET ALL IMAGES
DELIMITER //
DROP PROCEDURE IF EXISTS `images`;
CREATE PROCEDURE `images` ()
BEGIN 
  SELECT * FROM `image`;
END //

-- GET IMAGE
DROP PROCEDURE IF EXISTS `get_image`;
CREATE PROCEDURE `get_image` (
  IN p_path VARCHAR(255),
  OUT result VARCHAR(255)
)
BEGIN
  DECLARE b_id INT;
  SELECT EXISTS (
    SELECT `id` FROM `image` WHERE `path` = p_path
  ) INTO b_id;

  IF (b_id = 1) THEN 
    SELECT * FROM `image` WHERE `path` = p_path;
  ELSE 
    SET result = 0;
    SELECT result;
  END IF; 
END //

-- ADD NEW IMAGE
DROP PROCEDURE IF EXISTS `add_image`;
CREATE PROCEDURE `add_image` (
  IN p_path VARCHAR(255),
  OUT result VARCHAR(255)
) 
BEGIN
  DECLARE id_image INT;

  SELECT EXISTS (
    SELECT `id` FROM `image` WHERE `path` = p_path
  ) INTO id_image;

  IF (id_image = 0) THEN
    INSERT INTO `image`(`path`) VALUES(p_path);
    SET result = 1;
  ELSE 
    SET result = 0;
  END IF;
  SELECT result;
END //

-- DELETE IMAGE
DROP PROCEDURE IF EXISTS `delete_image`;
CREATE PROCEDURE `delete_image` (
  IN p_path VARCHAR(255),
  OUT result VARCHAR(255)
)
BEGIN 
  DECLARE b_id INT;
  DECLARE v_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `image` WHERE `path` = p_path
  ) INTO b_id;

  IF (b_id = 1) THEN
    SELECT `id` INTO v_id FROM `image` WHERE `path` = p_path;
    DELETE FROM `p_image` WHERE `id_image` = v_id;
    DELETE FROM `l_images` WHERE `id_image` = v_id;
    DELETE FROM `image` WHERE `id` = v_id;
    SET result = 1;
  ELSE
    SET result = 0;
  END IF;
  SELECT result;
END //
