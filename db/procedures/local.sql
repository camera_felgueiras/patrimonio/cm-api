DELIMITER //

-- GET ALL LOCAL
DROP PROCEDURE IF EXISTS `get_all_local`;
CREATE PROCEDURE `get_all_local` ()
BEGIN
  SELECT DISTINCT l.*, t.name as `type`, id_i.path, ci.tel as `phone`, ci.web as `website`, ci.email as `email`, ci.address as `address` FROM `local` l
    INNER JOIN `contact_info` ci ON ci.id = l.id_contact_info
    INNER JOIN `image` id_i ON id_i.id = l.id_main_image 
    INNER JOIN `l_category` lc ON lc.id_local = l.id
    INNER JOIN `category` c ON c.id = lc.id_category
    INNER JOIN `type` t ON t.id = c.id_type;
END //

-- GET LOCAL
DROP PROCEDURE IF EXISTS `get_local`;
CREATE PROCEDURE `get_local` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE v_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `local` WHERE `id` = p_id
  ) INTO v_id; 

  SET result = 0;
  IF (v_id = 1) THEN
    SELECT l.*, i.path, ci.tel as `phone`, ci.web as `website`, ci.email, ci.address FROM `local` l
      INNER JOIN `image` i ON i.id = l.id_main_image
      INNER JOIN `contact_info` ci ON ci.id = l.id_contact_info 
      WHERE l.id = p_id;
  ELSE 
    SELECT result;
  END IF;
END //

-- GET IMAGES
DROP PROCEDURE IF EXISTS `get_images_local`;
CREATE PROCEDURE `get_images_local` (
  IN p_id INT
)
BEGIN
  SELECT i.id, i.path, i.path as `name` FROM `image` i INNER JOIN `local` l ON l.id_main_image = i.id WHERE l.id = p_id;
  SELECT i.id, i.path, i.path as `name` FROM `image` i INNER JOIN `l_images` li ON li.id_image = i.id WHERE li.id_local = p_id;
END //

-- GET RELATED LOCAL
DROP PROCEDURE IF EXISTS `get_related_local`;
CREATE PROCEDURE `get_related_local` (
  IN p_id INT
)
BEGIN
  SELECT p.*, i.path FROM `p_has_l` pl
    INNER JOIN `personality` p ON p.id = pl.id_personality
    INNER JOIN `image` i ON i.id = p.profile_pic_id 
    WHERE pl.id_local = p_id;
  SELECT l.*, i.path, t.name as `type` FROM `l_has_l` ll
    INNER JOIN `local` l ON l.id = ll.id_local
    INNER JOIN `image` i ON i.id = l.id_main_image
    INNER JOIN `l_category` lc ON lc.id_local = l.id
    INNER JOIN `category` c ON c.id = lc.id_category
    INNER JOIN `type` t ON t.id = c.id_type
    WHERE ll.id_local = p_id;
  SELECT r.*, i.path, t.name as `type` FROM `l_has_r` lr
    INNER JOIN `route` r ON r.id = lr.id_route
    INNER JOIN `image` i ON i.id = r.id_main_image
    INNER JOIN `r_category` rc ON rc.id_route = r.id
    INNER JOIN `category` c ON c.id = rc.id_category
    INNER JOIN `type` t ON t.id = c.id_type
    WHERE lr.id_local = p_id;
END //

-- GET LOCAL _ HAS _ PERSONALITIES
DROP PROCEDURE IF EXISTS `get_l_has_p`;
CREATE PROCEDURE `get_l_has_p` (
  IN p_id INT
)
BEGIN
  SELECT lp.id_personality as related_id, p.name as related_name, i.path as related_image FROM `p_has_l` lp
    INNER JOIN `personality` p ON p.id = lp.id_personality
    INNER JOIN `image` i ON i.id = p.profile_pic_id
    WHERE lp.id_local = p_id;
END //

-- GET LOCAL _ HAS _ LOCAL
DROP PROCEDURE IF EXISTS `get_l_has_l`;
CREATE PROCEDURE `get_l_has_l` (
  IN p_id INT
)
BEGIN
  SELECT ll.id_l as related_id, l.title as related_name, i.path as related_image FROM `l_has_l` ll
    INNER JOIN `local` l ON l.id = ll.id_l
    INNER JOIN `image` i ON i.id = l.id_main_image 
    WHERE ll.id_local = p_id;
END //

-- GET BY TYPE
DROP PROCEDURE IF EXISTS `get_local_by_type`;
CREATE PROCEDURE `get_local_by_type` (
  IN p_type VARCHAR(255),
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE v_type INT;

  SELECT EXISTS (
    SELECT `id` FROM `type` WHERE `name` = p_type 
  ) INTO v_type;

  SET result = 0;
  IF (v_type = 1) THEN
    SET result = 1;
    SELECT DISTINCT result, l.id, l.title, l.likes, t.name as `type`, id_i.path FROM `local` l
      INNER JOIN `image` id_i ON id_i.id = l.id_main_image 
      INNER JOIN `l_category` lc ON lc.id_local = l.id
      INNER JOIN `category` c ON c.id = lc.id_category
      INNER JOIN `type` t ON t.id = c.id_type
      WHERE t.name = p_type;
  END IF;
  SELECT result;
END //

-- GET CATEGORY LOCAL
DROP PROCEDURE IF EXISTS `get_category_local`;
CREATE PROCEDURE `get_category_local`(
  IN p_id_local INT
)
BEGIN
  SELECT c.* FROM `l_category` lc 
    INNER JOIN `category` c ON c.id = lc.id_category
    WHERE lc.id_local = p_id_local;
END //

-- CREATE LOCAL
DROP PROCEDURE IF EXISTS `create_local`;
CREATE PROCEDURE `create_local` (
  IN p_title VARCHAR(255),
  IN p_likes INT,
  IN p_lat DOUBLE,
  IN p_lng DOUBLE,
  IN p_desc TEXT,
  IN id_contacts INT,
  IN p_main_image VARCHAR(255)
)
BEGIN 
  DECLARE v_profile_pic_id INT;
  SELECT `id` INTO v_profile_pic_id FROM `image` WHERE `path` = p_main_image;
  INSERT INTO `local`(`title`, `likes`, `lat`, `lng`, `desc`, `id_contact_info`, `id_main_image`) VALUES(p_title, p_likes, p_lat, p_lng, p_desc, id_contacts, v_profile_pic_id);
  SELECT `id` FROM `local` ORDER BY `id` DESC LIMIT 1;
END //

-- ADD RELATION
DROP PROCEDURE IF EXISTS `add_l_relation`;
CREATE PROCEDURE `add_l_relation` (
  IN id_l INT,
  IN id_to_add INT,
  IN p_type VARCHAR(255)
)
BEGIN
  IF (p_type = 'route') THEN 
    INSERT INTO `l_has_r`(`id_local`, `id_route`) VALUES(id_l, id_to_add);
  ELSEIF (p_type = 'personality') THEN
    INSERT INTO `p_has_l`(`id_personality`, `id_local`) VALUES(id_to_add, id_l);
  ELSE 
    INSERT INTO `l_has_l`(`id_local`, `id_l`) VALUES(id_l, id_to_add);
    INSERT INTO `l_has_l`(`id_local`, `id_l`) VALUES(id_to_add, id_l);
  END IF;
END //

-- ADD CATEGORY LOCAL
DROP PROCEDURE IF EXISTS `add_category_local`;
CREATE PROCEDURE `add_category_local` (
  IN p_id_local INT,
  IN p_id_category INT
)
BEGIN
  INSERT INTO `l_category`(`id_category`, `id_local`) VALUES(p_id_category, p_id_local); 
END //

-- ADD IMAGE LOCAL
DROP PROCEDURE IF EXISTS `add_image_local`;
CREATE PROCEDURE `add_image_local` (
  IN p_id_local INT,
  IN p_path VARCHAR(255)
)
BEGIN
  DECLARE b_i INT;
  DECLARE image_id INT;

  SELECT `id` INTO image_id FROM `image` WHERE `path` = p_path;
  SELECT EXISTS (
    SELECT `id` FROM `l_images` WHERE `id_image` = image_id
  ) INTO b_i;

  IF (b_i = 0) THEN
    INSERT INTO `l_images`(`id_local`, `id_image`) VALUES(p_id_local, image_id);
  END IF;
END //

-- EXISTS 
DROP PROCEDURE IF EXISTS `local_exists`;
CREATE PROCEDURE `local_exists` (
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_l INT;
  SELECT EXISTS (
    SELECT `id` FROM `local` WHERE `id` = p_id
  ) INTO b_l;

  SET result = 0;
  IF (b_l = 1) THEN
    SET result = 1;
  END IF;
  SELECT result;
END //

-- DELETE LOCAL IMAGE
DROP PROCEDURE IF EXISTS `delete_l_image`;
CREATE PROCEDURE `delete_l_image` (
  IN p_path VARCHAR(255) 
)
BEGIN
  DECLARE img_id INT;
  SELECT `id` INTO img_id FROM `image` WHERE `path` = p_path;
  DELETE FROM `l_images` WHERE `id_image` = img_id;
  DELETE FROM `image` WHERE `id` = img_id;
END //

-- GET TIMELINE LOCAL
DROP PROCEDURE IF EXISTS `get_timeline_l`;
CREATE PROCEDURE `get_timeline_l` (
  IN p_id INT
)
BEGIN
  SELECT t.*, i.path as `image` FROM `timeline` t 
    INNER JOIN `timeline_l` tl ON tl.id_t = t.id
    INNER JOIN `image` i ON i.id = t.id_image
    WHERE tl.id_l = p_id;  
END //

-- DELETE LOCAL CATEGORIES 
DROP PROCEDURE IF EXISTS `delete_l_categories`;
CREATE PROCEDURE `delete_l_categories` (
  IN p_id INT
)
BEGIN
  DELETE FROM `l_category` WHERE `id_local` = p_id;
END //

-- DELETE LOCAL TIMELINE
DROP PROCEDURE IF EXISTS `delete_timeline_l`;
CREATE PROCEDURE `delete_timeline_l`(
  IN p_id VARCHAR(255)
)
BEGIN
  DECLARE v_image INT;
  DELETE FROM `timeline_l` WHERE `id_t` = p_id; 
  SELECT `id_image` INTO v_image FROM `timeline` WHERE `id` = p_id;  
  DELETE FROM `timeline` WHERE `id` = p_id;
  DELETE FROM `image` WHERE `id` = v_image;
END // 

-- DELETE LOCAL RELATED
DROP PROCEDURE IF EXISTS `delete_local_related`;
CREATE PROCEDURE `delete_local_related` (
  IN p_id INT
)
BEGIN
  DELETE FROM `l_has_l` WHERE `id_local` = p_id;
  DELETE FROM `l_has_l` WHERE `id_l` = p_id;
  DELETE FROM `l_has_r` WHERE `id_local` = p_id;
  DELETE FROM `p_has_l` WHERE `id_local` = p_id;
END //

-- DELETE LOCAL
DROP PROCEDURE IF EXISTS `delete_local`;
CREATE PROCEDURE `delete_local` (
  IN p_id INT
)
BEGIN
  DECLARE v_contact INT;
  DECLARE v_main_image INT;
  DECLARE v_main_path VARCHAR(255);

  SELECT `id_main_image` INTO v_main_image FROM `local` WHERE `id` = p_id;
  SELECT `path` INTO v_main_path FROM `image` WHERE `id` = v_main_image;
  SELECT `id_contact_info` INTO v_contact FROM `local` WHERE `id` = p_id;

  DELETE FROM `path` WHERE `id_local` = p_id;
  DELETE FROM `l_category` WHERE `id_local` = p_id;
  CALL delete_local_related(p_id);

  DELETE FROM `local` WHERE `id` = p_id;
  DELETE FROM `contact_info` WHERE `id` = v_contact; 
  DELETE FROM `image` WHERE `id` = v_main_image;

  SELECT v_main_path as `pic`;
END //

-- UPDATE LOCAL
DROP PROCEDURE IF EXISTS `update_local`;
CREATE PROCEDURE `update_local` (
  IN p_id INT,
  IN p_title VARCHAR(255),
  IN p_lat DOUBLE,
  IN p_lng DOUBLE,
  IN p_desc TEXT,
  IN p_main_image VARCHAR(255),
  OUT result VARCHAR(1)
) 
BEGIN
  DECLARE b_l INT;
  DECLARE v_image INT;
  DECLARE image_path VARCHAR(255);

  SELECT EXISTS (
    SELECT `id` FROM `local` WHERE `id` = p_id
  ) INTO b_l;

  SET result = 0;
  IF (b_l = 1) THEN
    SELECT i.path INTO image_path FROM `image` i 
      INNER JOIN `local` p ON p.id_main_image = i.id
    WHERE p.id = p_id;
    SELECT `id` INTO v_image FROM `image` WHERE `path` = p_main_image; 
    UPDATE `local` SET `title` = p_title, `lat` = p_lat, `lng` = p_lng, `desc` = p_desc, `id_main_image` = v_image WHERE `id` = p_id; 
    SET result = 1;
  END IF;
  SELECT result;
  SELECT image_path;
END //
DELIMITER ;