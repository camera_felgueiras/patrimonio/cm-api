DELIMITER //

-- GET ALL ROUTES
DROP PROCEDURE IF EXISTS `routes`;
CREATE PROCEDURE `routes`()
BEGIN 
  SELECT r.*, i.path FROM `route` r 
    INNER JOIN `image` i ON i.id = r.id_main_image;
END //

-- GET ROUTE BY ID
DROP PROCEDURE IF EXISTS `get_route`;
CREATE PROCEDURE `get_route`(
  IN p_id INT,
  OUT result VARCHAR(1)
)
BEGIN
  DECLARE b_p_id INT;

  SELECT EXISTS (
    SELECT `id` FROM `route` WHERE `id` = p_id 
  ) INTO b_p_id;

  SET result = 0;
  IF (b_p_id = 1) THEN
    SELECT r.* FROM `route` r WHERE `id` = p_id;
  ELSE 
    SELECT result;
  END IF;
END //

-- CREATE A ROUTE
DROP PROCEDURE IF EXISTS `create_route`;
CREATE PROCEDURE `create_route`(
  IN p_title VARCHAR(255),
  IN p_desc TEXT,
  IN p_website VARCHAR(255),
  IN p_km INT,
  IN p_likes INT,
  IN p_main_image VARCHAR(255)
)
BEGIN
  DECLARE v_image_id INT;

  SELECT `id` INTO v_image_id FROM `image` WHERE `path` = p_main_image;

  INSERT INTO `route`(`title`, `likes`, `km`, `website`, `desc`, `id_main_image`) VALUES(p_title, p_likes, p_km, p_website, p_desc, v_image_id);
  SELECT `id` FROM `route` ORDER BY `id` DESC LIMIT 1;
END //

-- GET CATEGORY ROUTE 
DROP PROCEDURE IF EXISTS `get_category_route`;
CREATE PROCEDURE `get_category_route`(
  IN p_id_route INT
)
BEGIN
  SELECT c.* FROM `r_category` rc 
    INNER JOIN `category` c ON c.id = rc.id_category
    WHERE lc.id_route = p_id_route;
END //

-- ADD CATEGORY ROUTE
DROP PROCEDURE IF EXISTS `add_category_route`;
CREATE PROCEDURE `add_category_route` (
  IN p_id_route INT,
  IN p_id_category INT
)
BEGIN
  INSERT INTO `r_category`(`id_category`, `id_route`) VALUES(p_id_category, p_id_route); 
END //

-- ADD IMAGE ROUTE
DROP PROCEDURE IF EXISTS `add_image_route`;
CREATE PROCEDURE `add_image_route`(
  IN p_id_route INT,
  IN p_path VARCHAR(255)
)
BEGIN
  DECLARE id_image INT;

  SELECT `id` INTO id_image FROM `image` WHERE `path` = p_path;
  INSERT INTO `r_image`(`id_route`, `id_image`) VALUES(p_id_route, id_image);
END //

-- ADD CREATE PATH
DROP PROCEDURE IF EXISTS `create_path`;
CREATE PROCEDURE `create_path`(
  IN p_id_route INT,
  IN p_id_local INT
)
BEGIN
  INSERT INTO `path`(`id_local`, `id_route`) VALUES(p_id_local, p_id_route);
END //

-- ADD RELATION
DROP PROCEDURE IF EXISTS `add_r_relation`;
CREATE PROCEDURE `add_r_relation` (
  IN id_r INT,
  IN id_to_add INT,
  IN p_type VARCHAR(255)
)
BEGIN
  IF (p_type = 'route') THEN 
    INSERT INTO `r_has_r`(`id_route`, `id_r`) VALUES(id_r, id_to_add);
    INSERT INTO `r_has_r`(`id_route`, `id_r`) VALUES(id_to_add, id_r);
  ELSEIF (p_type = 'personality') THEN
    INSERT INTO `p_has_r`(`id_personality`, `id_route`) VALUES(id_to_add, id_r);
  ELSE 
    INSERT INTO `l_has_r`(`id_local`, `id_route`) VALUES(id_to_add, id_r);
  END IF;
END //

-- GET CATEGORY ROUTE
DROP PROCEDURE IF EXISTS `get_category_route`;
CREATE PROCEDURE `get_category_route`(
  IN p_route_id INT
)
BEGIN
  SELECT c.* FROM `category` c 
    INNER JOIN `r_category` rc ON rc.id_category = c.id
    INNER JOIN `route` r ON r.id = rc.id_route
  WHERE r.id = p_route_id;
END //

-- GET IMAGES
DROP PROCEDURE IF EXISTS `get_route_images`;
CREATE PROCEDURE `get_route_images` (
  IN p_id INT
)
BEGIN
  SELECT i.*, i.path as `name` FROM `image` i 
    INNER JOIN `route` r ON r.id_main_image = i.id 
  WHERE r.id = p_id; 
  SELECT i.*, i.path as `name` FROM `image` i
    INNER JOIN `r_image` ri ON ri.id_image = i.id
    INNER JOIN `route` r ON r.id = ri.id_route
  WHERE r.id = p_id;
END //

-- GET RELATED LOCAL
DROP PROCEDURE IF EXISTS `get_related_route`;
CREATE PROCEDURE `get_related_route` (
  IN p_id INT
)
BEGIN
  SELECT p.*, i.path FROM `p_has_r` pr
    INNER JOIN `personality` p ON p.id = pr.id_personality
    INNER JOIN `image` i ON i.id = p.profile_pic_id 
    WHERE pr.id_route = p_id;
  SELECT l.*, i.path, t.name as `type` FROM `l_has_r` lr
    INNER JOIN `local` l ON l.id = lr.id_local
    INNER JOIN `image` i ON i.id = l.id_main_image
    INNER JOIN `l_category` lc ON lc.id_local = l.id
    INNER JOIN `category` c ON c.id = lc.id_category
    INNER JOIN `type` t ON t.id = c.id_type
    WHERE lr.id_route = p_id;
  SELECT r.*, i.path, t.name as `type` FROM `r_has_r` rr
    INNER JOIN `route` r ON r.id = rr.id_route
    INNER JOIN `image` i ON i.id = r.id_main_image
    INNER JOIN `r_category` rc ON rc.id_route = r.id
    INNER JOIN `category` c ON c.id = rc.id_category
    INNER JOIN `type` t ON t.id = c.id_type
    WHERE rr.id_route = p_id;
END //

-- GET ROUTE PATH
DROP PROCEDURE IF EXISTS `get_path`;
CREATE PROCEDURE `get_path` (
  IN p_id INT
)
BEGIN
  SELECT l.*, i.path, t.name as `type` FROM `path` p
    INNER JOIN `local` l ON l.id = p.id_local
    INNER JOIN `image` i ON i.id = l.id_main_image
    INNER JOIN `l_category` lc ON lc.id_local = l.id
    INNER JOIN `category` c ON c.id = lc.id_category
    INNER JOIN `type` t ON t.id = c.id_type
    WHERE p.id_route = p_id;
END //

-- GET ROUTE HAS LOCAL
DROP PROCEDURE IF EXISTS `get_r_has_l`;
CREATE PROCEDURE `get_r_has_l`(
  IN p_id INT
)
BEGIN
  SELECT lr.id_local as related_id, l.title as related_name, i.path as related_image FROM `l_has_r` lr
    INNER JOIN `local` l ON l.id = lr.id_local
    INNER JOIN `image` i ON i.id = l.id_main_image
    WHERE lr.id_route = p_id;
END //

-- GET ROUTE HAS PERSONALITY
DROP PROCEDURE IF EXISTS `get_r_has_p`;
CREATE PROCEDURE `get_r_has_p`(
  IN p_id INT
)
BEGIN
  SELECT pr.id_personality as related_id, p.name as related_name, i.path as related_image FROM `p_has_r` pr 
    INNER JOIN `personality` p ON p.id = pr.id_personality
    INNER JOIN `image` i ON i.id = p.profile_pic_id
    WHERE pr.id_route = p_id;
END //

-- ROUTE EXISTS
DROP PROCEDURE IF EXISTS `route_exists`;
CREATE PROCEDURE `route_exists` (
  IN p_id INT,
  OUT result INT
)
BEGIN
  DECLARE b_r INT;
  SELECT EXISTS (
    SELECT `id` FROM `route` WHERE `id` = p_id
  ) INTO b_r;

  SET result = 0;
  IF (b_r = 1) THEN
    SET result = 1;
  END IF;
  SELECT result;
END //

-- DELETE IMAGE
DROP PROCEDURE IF EXISTS `delete_r_image`;
CREATE PROCEDURE `delete_r_image` (
  IN p_path VARCHAR(255)
)
BEGIN
  DECLARE img_id INT;
  SELECT `id` INTO img_id FROM `image` WHERE `path` = p_path;
  DELETE FROM `r_image` WHERE `id_image` = img_id;
  DELETE FROM `image` WHERE `id` = img_id;
END //

-- DELETE ROUTE
DROP PROCEDURE IF EXISTS `delete_route`;
CREATE PROCEDURE `delete_route` (
  IN p_id INT
)
BEGIN
  DECLARE v_main_image INT;
  DECLARE v_main_path VARCHAR(255);

  SELECT `id_main_image` INTO v_main_image FROM `route` WHERE `id` = p_id;
  SELECT `path` INTO v_main_path FROM `image` WHERE `id` = v_main_image;

  DELETE FROM `path` WHERE `id_route` = p_id;
  DELETE FROM `r_category` WHERE `id_route` = p_id;
  CALL delete_route_related(p_id);

  DELETE FROM `route` WHERE `id` = p_id;
  DELETE FROM `image` WHERE `id` = v_main_image;

  SELECT v_main_path as `pic`;
END //

-- DELETE RELATED ROUTE
DROP PROCEDURE IF EXISTS `delete_route_related`; 
CREATE PROCEDURE `delete_route_related` (
  IN p_id INT
)
BEGIN
  DELETE FROM `r_has_r` WHERE `id_route` = p_id; 
  DELETE FROM `r_has_r` WHERE `id_r` = p_id; 
  DELETE FROM `l_has_r` WHERE `id_route` = p_id; 
  DELETE FROM `p_has_r` WHERE `id_route` = p_id; 
END //

-- DELETE ROUTE CATEGORIES 
DROP PROCEDURE IF EXISTS `delete_r_categories`;
CREATE PROCEDURE `delete_r_categories` (
  IN p_id INT
)
BEGIN
  DELETE FROM `r_category` WHERE `id_route` = p_id;
END //

-- UPDATE ROUTE
DROP PROCEDURE IF EXISTS `update_route`;
CREATE PROCEDURE `update_route` (
  IN p_id INT,
  IN p_title VARCHAR(255),
  IN p_desc TEXT,
  IN p_website VARCHAR(255),
  IN p_km VARCHAR(255),
  IN p_main_image VARCHAR(255),  
  OUT result VARCHAR(1)
) 
BEGIN
  DECLARE b_l INT;
  DECLARE v_image INT;
  DECLARE image_path VARCHAR(255);

  SELECT EXISTS (
    SELECT `id` FROM `route` WHERE `id` = p_id
  ) INTO b_l;

  SET result = 0;
  IF (b_l = 1) THEN
    SELECT i.path INTO image_path FROM `image` i 
      INNER JOIN `route` r ON r.id_main_image = i.id
    WHERE r.id = p_id;
    SELECT `id` INTO v_image FROM `image` WHERE `path` = p_main_image; 
    UPDATE `route` SET `title` = p_title, `km` = p_km, `website` = p_website, `desc` = p_desc, `id_main_image` = v_image WHERE `id` = p_id; 
    SET result = 1;
  END IF;
  SELECT result;
  SELECT image_path;
END //
DELIMITER ;
