const express = require('express');
const logger = require('morgan');
const app = express();
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const multer = require('multer');
const bodyParser = require('body-parser');

const CONFIG = require('./config/config');
const jwt = require('./config/jwt');
//const jwt = require('express-jwt');

app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', jwt());

const routePath = path.join(__dirname, 'routes');
fs.readdirSync(routePath).forEach(element => {
  const file = require(path.join(routePath, element));
  app.use(`/api/${file.name}`, file.router);
});

app.use((err, req, res, next) => {
  res.status(err.status || 500).send({ message: err.message, error: err });
});

app.listen(CONFIG.port);
console.log('App listening on: ', CONFIG.port);
