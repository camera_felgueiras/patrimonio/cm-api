const express = require('express');
const router = express.Router();
const Timeline = require('../models/Timeline');
const Image = require('../models/Image');
const fs = require('fs');
const path = require('path');

const imagePath = path.join(__dirname, '..', 'uploads', 'images');

router.post('/', (req, res) => {
  const fact = {
    date: req.body.date,
    desc: req.body.desc,
    title: req.body.title,
    image: req.body.image
  };
  const related = req.body.related;

  if (related.type === 'heritage') {
    Timeline.createLocalTimeline(fact, related.id, err => {
      if (err) return res.status(500).send({ message: 'SQL error adding timeline', err });
      res.send({ message: 'Timeline Criada', timeline: fact });
    });
  } else if (related.type === 'personality') {
    Timeline.createPersonalityTimeline(fact, related.id, err => {
      if (err) return res.status(500).send({ message: 'SQL error adding timeline', err });
      res.send({ message: 'Timeline Criada', timeline: fact });
    });
  } else {
    res.status(404).send({ message: 'Tipo relacionado errado' });
  }
});

router.get('/:id?', (req, res) => {
  const id = req.params.id;
  if (id) {
    Timeline.getById(id, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting timeline by id', err });

      const result = +rows[0][0].result;
      if (result === 0) {
        res.status(404).send({ message: 'Timeline não existe' });
      } else {
        const timeline = rows[0][0];
        res.send({ timeline });
      }
    });
  } else {
    Timeline.get((err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting timeline', err });
      const timeline = {};
      timeline.personalities = rows[0];
      timeline.locals = rows[1];

      if (timeline) {
        res.send({ timeline });
      } else {
        res.status(404).send({ message: 'Sem timeline' });
      }
    });
  }
});

router.get('/image/:id', (req, res) => {
  const id = req.params.id;

  Timeline.getImage(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error getting image' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Timeline não existe' });
    } else {
      res.send({ images: rows[0] });
    }
  });
});

router.get('/related/:id', (req, res) => {
  const id = req.params.id;

  Timeline.getRelated(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error getting related' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Timeline não existe' });
    } else {
      res.send({ related: rows[0] });
    }
  });
});

router.put('/:id', (req, res) => {
  const id = req.params.id;
  const fact = {
    id_t_related: req.body.id_t_related,
    date: req.body.date,
    desc: req.body.desc,
    title: req.body.title,
    image: req.body.image
  };
  const related = req.body.related;

  Timeline.update(id, fact, related, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error updating timeline', err });

    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Timeline não existe' });
    } else {
      const prev_image = rows[0][0].image_path;
      if (prev_image !== fact.image) {
        Image.delete(prev_image, (err, rows) => {
          if (err) return res.status(500).send({ message: 'SQL error deleting image' });
          const iPath = path.join(imagePath, prev_image);
          if (fs.existsSync(iPath)) {
            fs.unlinkSync(iPath, err => {
              if (err) return res.status(204).send({ message: 'An Error Ocurred' });
            });
          }
        });
      }
      res.send({ message: 'Timeline updated' });
    }
  });
});

router.delete('/:id', (req, res) => {
  const id = req.params.id;

  Timeline.delete(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error deleting timeline', err });

    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Timeline não existe' });
    } else {
      const image = rows[0][0].image_path;

      fs.unlinkSync(path.join(imagePath, image), err => {
        if (err) return res.status(204).send({ message: 'An Error Ocurred' });
      });

      res.send({ message: 'Timeline Removida' });
    }
  });
});
module.exports = { name: 'timeline', router };
