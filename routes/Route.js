const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const Route = require('../models/Route');
const Image = require('../models/Image');

const imagePath = path.join(__dirname, '..', 'uploads', 'images');

router.post('/', (req, res) => {
  const route = {
    title: req.body.title,
    desc: req.body.desc,
    website: req.body.website,
    km: req.body.km,
    likes: 0,
    image: req.body.image,
  };

  const categories = req.body.categories;
  const images = req.body.images;
  const relations = req.body.relations;
  const path = req.body.path;

  Route.create(route, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error creating route', err });
    const route_id = rows[0][0].id;
    if (categories && categories.length > 0) {
      categories.forEach(cat => {
        Route.addCategory(route_id, cat);
      });
    }
    if (images && images.length > 0) {
      images.forEach(img => {
        Route.addImage(route_id, img);
      });
    }
    if (path && path.length > 0) {
      path.forEach(p => {
        Route.createPath(route_id, p);
      });
    }
    if (relations && relations.length > 0) {
      relations.forEach(r => {
        Route.addRelation(route_id, r);
      });
    }
    res.send({ message: 'Rota Criada', route });
  });
});

router.get('/:id?', (req, res) => {
  const id = req.params.id;
  if (id) {
    Route.getById(id, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting route by id' });
      const result = +rows[0][0].result;
      if (result === 0) {
        return res.status(404).send({ message: 'Rota não existe' });
      }
      const route = rows[0][0];
      Route.getImages(route.id, (err, rows) => {
        route.images = rows[1].map(v => v.path);
        Route.getPath(route.id, (err, rows) => {
          route.path = rows[0];
          route.related = {};
          Route.getRelatedL(route.id, (err, rows) => {
            if (err) console.log(err);
            route.related.locals = rows[0];
            Route.getRelatedP(route.id, (err, rows) => {
              if (err) console.log(err);
              route.related.personalities = rows[0];
              Route.getCategory(route.id, (err, rows) => {
                route.categories = rows[0];
                return res.send({ route });
              });
            });
          });
        });
      });
    });
  } else {
    Route.get((err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting routes', err });
      const routes = rows[0];
      if (routes.length <= 0) {
        return res.status(404).send({ message: 'Sem rotas' });
      }
      routes.forEach((route, index) => {
        Route.getCategory(route.id, (err, rows) => {
          route.categories = rows[0];
          if (index === routes.length - 1) {
            return res.send({ routes });
          }
        });
      });
    });
  }
});

router.get('/images/:id', (req, res) => {
  const id = req.params.id;
  Route.getImages(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error getting images', err });
    res.send({ mainImage: rows[0][0], images: rows[1] });
  });
});

router.get('/related/:id', (req, res) => {
  const id = req.params.id;
  const related = {};
  Route.getRelated(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL erro getting related', err });
    related.personalities = rows[0];
    related.locals = rows[1];
    related.routes = rows[2];
    res.send({ related });
  });
});

router.get('/path/:id', (req, res) => {
  const id = req.params.id;
  Route.getPath(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error getting path', err });
    res.send({ path: rows[0] });
  });
});

router.put('/:id', (req, res) => {
  const id = req.params.id;

  const route = {
    title: req.body.title,
    desc: req.body.desc,
    website: req.body.website,
    km: req.body.km,
    image: req.body.image,
  };

  const categories = req.body.categories;
  const images = req.body.images;
  const relations = req.body.relations;
  const path = req.body.path;

  Route.exists(id, (err, rows) => {
    if (err) return res.send({ message: 'SQL error verifying route', err });

    Route.update(id, route, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error updating route', err });
      const prev_image = rows[1][0].image_path;
      if (prev_image !== route.image) {
        Image.delete(prev_image, (err, rows) => {
          if (err) return res.status(500).send({ message: 'SQL error deleting image', err });
          const iPath = path.join(imagePath, prev_image);
          if (fs.existsSync(iPath)) {
            fs.unlinkSync(iPath, err => {
              if (err) return res.status(204).send({ message: 'An Error Ocurred' });
            });
          }
        });
      }

      Route.getImages(id, (err, rows) => {
        if (err) return res.status(500).send({ message: 'SQL error getting images', err });
        if (rows[1].length > 0) {
          const images_db = rows[1].map(v => v.path);
          const toRemove = images_db.filter(i => !images.includes(i));

          if (toRemove.length > 0) {
            toRemove.forEach(i => {
              Route.deleteImage(i);
              const iPath = path.join(imagePath, i);
              if (fs.existsSync(iPath)) {
                fs.unlinkSync(iPath, err => {
                  if (err) return res.status(204).send({ message: 'An Error Ocurred' });
                });
              }
            });
          }
        }

        if (images && images.length > 0) {
          images.forEach(image => {
            Route.addImage(id, image);
          });
        }

        Route.deleteCategories(id, (err, rows) => {
          if (err) return res.status(500).send({ message: 'SQL error getting categories', err });
          categories.forEach(c => {
            Route.addCategory(id, c);
          });

          if (relations) {
            Route.deleteRelations(id, (err, rows) => {
              if (err)
                return res.status(500).send({ message: 'SQL error deleting relations', err });
              if (relations.length > 0) {
                relations.forEach(r => {
                  Route.addRelation(id, r);
                });
              }
            });
            res.send({ message: 'Rota Atualizado' });
          } else {
            res.send({ message: 'Rota Atualizado' });
          }
        });
      });
    });
  });
});

router.delete('/:id', (req, res) => {
  const id = req.params.id;

  Route.exists(id, (err, rows) => {
    if (err) return res.status(500).message({ message: 'SQL error verifying route', err });
    const result = +rows[0][0].result;
    if (result === 0) {
      return res.status(404).send({ message: 'Rota não existe' });
    } else {
      Route.getImages(id, (err, rows) => {
        if (err) return res.status(500).send({ message: 'SQL error deleting images', err });
        if (rows[1].length > 0) {
          const images = rows[1].map(v => v.path);
          images.forEach(i => {
            Route.deleteImage(i);
            const currentImage = path.join(imagePath, i);
            if (fs.existsSync(currentImage)) {
              fs.unlinkSync(currentImage, err => {
                if (err) return res.status(204).send({ message: 'An Error Ocurred' });
              });
            }
          });
        }

        Route.delete(id, (err, rows) => {
          if (err) return res.status(500).send({ message: 'SQL error deleting route', err });
          let image = rows[0][0].pic;
          const currentImage = path.join(imagePath, image);
          if (fs.existsSync(currentImage)) {
            fs.unlinkSync(currentImage, err => {
              if (err) return res.status(204).send({ message: 'An Error Ocurred', err });
            });
          }
          return res.send({ message: 'Rota Eliminada' });
        });
      });
    }
  });
});

module.exports = { name: 'route', router };
