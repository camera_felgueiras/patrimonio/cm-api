const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const Contacts = require('../models/Contacts');
const Timeline = require('../models/Timeline');
const Local = require('../models/Local');
const Image = require('../models/Image');

const imagePath = path.join(__dirname, '..', 'uploads', 'images');

router.post('/', (req, res) => {
  const contacts = {
    phone: req.body.phone,
    email: req.body.email,
    website: req.body.website,
    address: req.body.address
  };
  const local = {
    title: req.body.title,
    likes: 0,
    lat: req.body.lat,
    lng: req.body.lng,
    desc: req.body.desc,
    mainImage: req.body.mainImage
  };

  const categories = req.body.categories;
  const images = req.body.images;
  const relations = req.body.relations;
  const facts = req.body.facts;

  Contacts.create(contacts, (err, rows) => {
    if (err) res.status(500).send({ message: 'SQL error creating contact', err });
    local.contacts_id = rows[0][0].id;

    Local.create(local, (err, rows) => {
      if (err) res.status(500).send({ message: 'SQL error creating local', err });
      const local_id = rows[0][0].id;
      if (categories && categories.length > 0) {
        categories.forEach(cat => {
          Local.addCategory(local_id, cat);
        });
      }
      if (images && images.length > 0) {
        images.forEach(img => {
          Local.addImage(local_id, img);
        });
      }
      if (facts && facts.length > 0) {
        facts.forEach(fact => {
          Timeline.createLocalTimeline(fact, local_id);
        });
      }
      if (relations && relations.length > 0) {
        relations.forEach(r => {
          Local.addRelation(local_id, r);
        });
      }
      res.send({ message: 'Local Criado', local });
    });
  });
});

router.get('/', (req, res) => {
  const type = req.query.type;
  if (type) {
    Local.getByType(type, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SLQ error getting locals by type' });
      const result = +rows[0][0];
      if (result && result.result && result.result === 0) {
        res.status(404).send({ message: 'Tipo não existe' });
      } else {
        let locals = rows[0];
        if (locals.length > 0) {
          locals.forEach((local, index) => {
            Local.getCategory(local.id, (err, rows) => {
              locals[index].categories = rows[0];
              if (index === locals.length - 1) {
                res.send({ locals });
              }
            });
          });
        } else {
          res.status(404).send({ message: 'Sem locais' });
        }
      }
    });
  } else {
    Local.getAll((err, rows) => {
      if (err) res.status(err).send({ message: 'SQL error getting locals' });
      let locals = rows[0];
      let hasError;
      if (locals.length > 0) {
        locals.forEach((local, index) => {
          Local.getCategory(local.id, (err, rows) => {
            if (err) hasError = err;
            locals[index].categories = rows[0];
            if (index === locals.length - 1) {
              if (hasError) {
                res.send({ message: 'SQL error getting categories' });
              } else {
                res.send({ locals });
              }
            }
          });
        });
      } else {
        res.status(404).send({ message: 'Sem nenhum local' });
      }
    });
  }
});

router.get('/:id', (req, res) => {
  const id = req.params.id;
  Local.get(id, (err, rows) => {
    if (err) res.status(500).send({ message: 'SQL error getting local information' });

    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Local não encontrado' });
    } else {
      const local = rows[0][0];
      Local.getImages(local.id, (err, rows) => {
        if (err) return res.status(500).send({ message: 'SQL error getting local images' });
        local.images = rows[1].map(v => v.path);
        local.related = {};
        Local.getRelatedL(local.id, (err, rows) => {
          if (err)
            return res.status(500).send({ message: 'SQL error getting related locals', err });
          local.related.locals = rows[0];
          Local.getRelatedP(local.id, (err, rows) => {
            if (err)
              return res.status(500).send({ message: 'SQL error getting related personalities' });
            local.related.personalities = rows[0];
            Local.getCategory(local.id, (err, rows) => {
              local.categories = rows[0];
              res.send({ local });
            });
          });
        });
      });
    }
  });
});

router.get('/images/:id', (req, res) => {
  const id = req.params.id;
  Local.getImages(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error getting images', err });
    res.send({ mainImage: rows[0][0], images: rows[1] });
  });
});

router.get('/related/:id', (req, res) => {
  const id = req.params.id;
  const related = {};
  Local.getRelated(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL erro getting related', err });
    related.personalities = rows[0];
    related.locals = rows[1];
    related.routes = rows[2];
    res.send({ related });
  });
});

router.put('/:id', (req, res) => {
  const id = req.params.id;
  const contact = {
    phone: req.body.phone,
    email: req.body.email,
    website: req.body.website,
    address: req.body.address
  };
  const local = {
    title: req.body.title,
    lat: req.body.lat,
    lng: req.body.lng,
    desc: req.body.desc,
    mainImage: req.body.mainImage
  };
  const images = req.body.images;
  const relations = req.body.relations;
  const categories = req.body.categories;

  Local.exists(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error verifying local', err });

    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(409).send({ message: 'Local não existe' });
    } else {
      Contacts.updateLocal(id, contact, (err, rows) => {
        if (err) return res.status(500).send({ message: 'SQL error updating contacts', err });

        Local.update(id, local, (err, rows) => {
          if (err) return res.status(500).send({ message: 'SQL error updating local', err });

          const prev_image = rows[1][0].image_path;
          if (prev_image !== local.mainImage) {
            Image.delete(prev_image, (err, rows) => {
              if (err) return res.status(500).send({ message: 'SQL error deleting image', err });
              const iPath = path.join(imagePath, prev_image);
              if (fs.existsSync(iPath)) {
                fs.unlinkSync(iPath, err => {
                  if (err) return res.status(204).send({ message: 'An Error Ocurred' });
                });
              }
            });
          }

          Local.getImages(id, (err, rows) => {
            if (err) return res.status(500).send({ message: 'SQL error getting images', err });
            if (rows[1].length > 0) {
              const images_db = rows[1].map(v => v.path);
              const toRemove = images_db.filter(i => !images.includes(i));

              if (toRemove.length > 0) {
                toRemove.forEach(i => {
                  Local.deleteImage(i);
                  const iPath = path.join(imagePath, i);
                  if (fs.existsSync(iPath)) {
                    fs.unlinkSync(iPath, err => {
                      if (err) return res.status(204).send({ message: 'An Error Ocurred' });
                    });
                  }
                });
              }
            }

            if (images && images.length > 0) {
              images.forEach(image => {
                Local.addImage(id, image);
              });
            }

            Local.deleteCategories(id, (err, rows) => {
              if (err)
                return res.status(500).send({ message: 'SQL error getting categories', err });
              categories.forEach(c => {
                Local.addCategory(id, c);
              });

              if (relations) {
                Local.deleteRelations(id, (err, rows) => {
                  if (err)
                    return res.status(500).send({ message: 'SQL error deleting relations', err });
                  if (relations.length > 0) {
                    relations.forEach(r => {
                      Local.addRelation(id, r);
                    });
                  }
                });
                res.send({ message: 'Local Atualizado' });
              } else {
                res.send({ message: 'Local Atualizado' });
              }
            });
          });
        });
      });
    }
  });
});

router.delete('/:id', (req, res) => {
  const id = req.params.id;
  Local.exists(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error verifying existence', err });

    const result = +rows[0][0].result;
    if (result === 0) {
      return res.status(404).send({ message: 'Local não existe' });
    } else {
      Local.getImages(id, (err, rows) => {
        if (err) return res.status(500).send({ message: 'SQL error getting local images', err });
        if (rows[1].length > 0) {
          const images = rows[1].map(v => v.path);
          images.forEach(i => {
            Local.deleteImage(i);
            const currentImage = path.join(imagePath, i);
            if (fs.existsSync(currentImage)) {
              fs.unlinkSync(currentImage, err => {
                if (err) return res.status(204).send({ message: 'An Error Ocurred' });
              });
            }
          });
        }

        Local.getTimeline(id, (err, rows) => {
          if (err)
            return res.status(500).send({ message: 'SQL error getting local timeline', err });
          const timeline = rows[0];
          timeline.forEach(time => {
            Local.deleteTimeline(time.id);
            const currentImage = path.join(imagePath, time.image);
            if (fs.existsSync(currentImage)) {
              fs.unlinkSync(currentImage, err => {
                if (err) return res.status(204).send({ message: 'An Error Ocurred' });
              });
            }
          });

          Local.delete(id, (err, rows) => {
            if (err) return res.status(500).send({ message: 'SQL error deleting local', err });
            let image = rows[0][0].pic;
            const currentImage = path.join(imagePath, image);
            if (fs.existsSync(currentImage)) {
              fs.unlinkSync(currentImage, err => {
                if (err) return res.status(204).send({ message: 'An Error Ocurred', err });
              });
            }
            return res.send({ message: 'Local Eliminado' });
          });
        });
      });
    }
  });
});

module.exports = { name: 'local', router };
