const CONFIG = require('../config/config');
const express = require('express');
const router = express.Router();
const passHash = require('password-hash');
const jwt = require('jsonwebtoken');

const User = require('../models/User');

router.post('/', (req, res) => {
  const { email, password, firstName, lastName } = req.body;
  const cryptPass = passHash.generate(password);
  User.create({ email, cryptPass, firstName, lastName }, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL Error Creating user', err });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(409).send({ message: 'Email já em uso' });
    } else {
      res.status(200).send({
        message: 'User Criado',
        user: { email, firstName, lastName },
      });
    }
  });
});

router.post('/auth', (req, res) => {
  const { email, password } = req.body;
  User.auth(email, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL Error auth user', err });

    const result = +rows[0][0].result;
    const verify = passHash.verify(password, rows[0][0].passwd);

    if (result === 0 || !verify) {
      res.status(403).send({ message: 'Email ou Password Invalido' });
    } else {
      const user = {
        id: rows[0][0].id,
        email: rows[0][0].email,
        firstName: rows[0][0].first_name,
        lastName: rows[0][0].last_name,
        token: jwt.sign({ sub: rows[0][0].id }, CONFIG.jwt_encryption),
      };
      res.status(200).send({ user });
    }
  });
});

router.get('/:id?', (req, res) => {
  const id = req.params.id;
  if (id) {
    User.get(id, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL Error getting user' });
      const result = +rows[0][0].result;
      if (result === 0) {
        res.status(404).send({ message: 'Utilizador não existe' });
      } else {
        const user = {
          id: rows[0][0].id,
          email: rows[0][0].email,
          firstName: rows[0][0].first_name,
          lastName: rows[0][0].last_name,
        };
        res.status(200).send({ user });
      }
    });
  } else {
    User.getAll((err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL Error getting user' });
      const users = rows[0].map(u => {
        return {
          id: u.id,
          email: u.email,
          first_name: u.first_name,
          last_name: u.last_name,
        };
      });
      res.status(200).send({ users });
    });
  }
});

router.delete('/:id', (req, res) => {
  const id = req.params.id;
  User.delete(id, (err, rows) => {
    if (err) res.status(500).send({ message: 'SQL Error delete user' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Utilizador não existe' });
    } else {
      res.status(200).send({ message: 'Utilizador Removido' });
    }
  });
});

router.put('/', (req, res) => {
  const user = {
    id: req.body.id,
    email: req.body.email,
    password: req.body.password,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  };

  User.get(user.id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL Error validating user' });

    const result = +rows[0][0].result;
    const verify = passHash.verify(user.password, rows[0][0].passwd);

    if (result === 0) {
      res.status(404).send({ message: 'Utilizador não encontrado' });
    } else if (!verify) {
      res.status(403).send({ message: 'Password Invalida' });
    } else {
      User.update(user, (err, rows) => {
        if (err) res.status(500).send({ message: 'SQL Error update user' });
        const result = +rows[0][0].result || 1;

        if (result === 0) {
          res.status(409).send({ message: 'Email já em uso' });
        } else {
          const newUser = {
            id: rows[0][0].id,
            email: rows[0][0].email,
            firstName: rows[0][0].first_name,
            lastName: rows[0][0].last_name,
            token: 'fake-jwt-token',
          };
          res.status(200).send({ message: 'Utilizador Atualizado', user: newUser });
        }
      });
    }
  });
});

router.put('/passwd', (req, res) => {
  const user = {
    id: req.body.id,
    password: req.body.password,
    newPassword: req.body.newPassword,
  };

  User.get(user.id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL Error validating user' });

    const result = +rows[0][0].result;
    const verify = passHash.verify(user.password, rows[0][0].passwd);

    if (result === 0) {
      res.status(404).send({ message: 'Utilizador não encontrado' });
    } else if (!verify) {
      res.status(403).send({ message: 'Password Invalida' });
    } else {
      const newPass = passHash.generate(user.newPassword);
      User.updatePasswd(user.id, newPass, (err, rows) => {
        if (err) return res.status(500).send({ message: 'SQL Error updating password' });
        const result = +rows[0][0].result;

        if (result === 0) {
          res.status(409).send({ message: 'Erro ao atualizar password' });
        } else {
          res.status(200).send({ message: 'Password atualizada com sucesso' });
        }
      });
    }
  });
});

module.exports = { name: 'user', router };
