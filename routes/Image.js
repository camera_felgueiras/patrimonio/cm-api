const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const sharp = require('sharp');
const uuid = require('uuid');
const multer = require('multer');

const Image = require('../models/Image');

const uploadPath = path.join(__dirname, '..', 'uploads');
const imagePath = path.join(__dirname, '..', 'uploads', 'images');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (!fs.existsSync(uploadPath)) {
      fs.mkdirSync(uploadPath);
      fs.mkdirSync(imagePath);
    } else if (!fs.existsSync(imagePath)) {
      fs.mkdirSync(imagePath);
    }
    cb(null, imagePath);
  },
  filename: (req, file, cb) => {
    cb(null, uuid.v4() + path.extname(file.originalname));
  }
});

const limits = {
  fileSize: 1024 * 1024 * 10,
  onFileSizeLimit: err => {
    next(err);
  }
};

const upload = multer({
  storage,
  limits
});

const mapImages = array => {
  if (array === undefined) {
    return [];
  }
  return array.map(i => {
    return {
      name: i.originalname,
      url: i.filename
    };
  });
};

const makeThumbnail = (path, width, height) => {
  const readStream = fs.createReadStream(path);
  let transform = sharp();
  transform.resize(width, height);
  return readStream.pipe(transform);
};

router.post(
  '/',
  upload.fields([{ name: 'image', maxCount: 1 }, { name: 'images', maxCount: 8 }]),
  (req, res) => {
    const image = req.files.image;
    const images = req.files.images;
    const mappedImage = mapImages(image);
    const mappedImages = mapImages(images);
    const joined = mappedImage.concat(mappedImages);
    joined.forEach(ele => Image.add(ele.url, () => {}));
    res.status(201).send(joined);
  }
);

router.delete('/:image', (req, res) => {
  const name = req.params.image;
  const filePath = path.join(imagePath, name);
  Image.delete(name, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL erro deleting image', err });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Imagem não encontrada' });
    } else {
      if (fs.existsSync(filePath)) {
        fs.unlinkSync(filePath, err => {
          if (err) return res.status(204).send({ message: 'An Error Ocurred' });
        });
      }
      res.send({ message: 'Imagem eliminada', filename: name });
    }
  });
});

router.get('/:image', (req, res) => {
  const name = req.params.image;
  const filePath = path.join(imagePath, name);
  if (!fs.existsSync(filePath)) {
    res.status(404).send({ message: 'Ficheiro não encontrado' });
  } else {
    res.sendFile(filePath);
  }
});

router.get('/th/:image', async (req, res) => {
  const w = +req.query.w || 240;
  const h = +req.query.h || 240;
  const name = req.params.image;
  const filePath = path.join(imagePath, name);
  if (!fs.existsSync(filePath)) {
    res.status(404).send({ message: 'Ficheiro não encontrado' });
  } else {
    res.type('image/*');
    makeThumbnail(filePath, w, h).pipe(res);
  }
});

module.exports = { name: 'image', router };
