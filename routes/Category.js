const express = require('express');
const router = express.Router();
const Category = require('../models/Category');

router.post('/', (req, res) => {
  const category = {
    name: req.body.name,
    type: req.body.type
  };
  Category.create(category, (err, rows) => {
    if (err) return res.status(500).send({ err, message: 'SQL error adding category' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Tipo não existe' });
    } else if (result === 1) {
      res.status(409).send({ message: 'Categoria já existe' });
    } else {
      res.send({ message: 'Adicionada nova categoria', category });
    }
  });
});

router.get('/', (req, res) => {
  const type = req.query.type;
  if (type) {
    Category.getByType(type, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting categories by type' });
      const result = +rows[0][0].result;
      if (result === 0) {
        res.status(404).send({ message: 'Tipo não existe' });
      } else {
        res.send({ categories: rows[0] });
      }
    });
  } else {
    Category.getAll((err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting all types' });
      res.send({ categories: rows[0] });
    });
  }
});

router.get('/:id', (req, res) => {
  const id = req.params.id;
  Category.get(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error getting type' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Categoria não existe' });
    } else {
      res.send({ category: rows[0][0] });
    }
  });
});

router.delete('/:id', (req, res) => {
  Category.delete(req.params.id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error deleting type' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Categoria não existe' });
    } else {
      res.send({ message: 'Categoria removida' });
    }
  });
});

module.exports = { name: 'category', router };
