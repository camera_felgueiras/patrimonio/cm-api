const express = require('express');
const router = express.Router();
const Type = require('../models/Type');

router.post('/', (req, res) => {
  const type = {
    name: req.body.name
  };

  Type.add(type, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error adding new type' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(409).send({ message: 'Tipo já existe' });
    } else {
      res.send({ message: 'Tipo criado', type });
    }
  });
});

router.get('/:id?', (req, res) => {
  const id = req.params.id;
  if (id) {
    Type.get(id, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting type' });
      const result = +rows[0][0].result;
      if (result === 0) {
        res.status(404).send({ message: 'Tipo não encontrado' });
      } else {
        res.send({ type: rows[0][0] });
      }
    });
  } else {
    Type.getAll((err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting all types' });
      res.send({ types: rows[0] });
    });
  }
});

router.delete('/:id', (req, res) => {
  const id = req.params.id;
  Type.delete(id, (err, rows) => {
    if (err) return res.status(500).send({ err, message: 'SQL error deleting type' });
    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(404).send({ message: 'Tipo não encontrado' });
    } else {
      res.send({ message: 'Tipo eliminado' });
    }
  });
});
module.exports = { name: 'type', router };
