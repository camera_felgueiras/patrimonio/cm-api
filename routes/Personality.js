const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const Personality = require('../models/Personality');
const Timeline = require('../models/Timeline');
const Image = require('../models/Image');

const imagePath = path.join(__dirname, '..', 'uploads', 'images');

router.post('/', (req, res) => {
  const personality = {
    name: req.body.name,
    job: req.body.job,
    birth: req.body.birth,
    death: req.body.death,
    desc: req.body.desc,
    profile_pic: req.body.profile_pic,
  };
  const images = req.body.images;
  const facts = req.body.facts;
  const relations = req.body.relations;

  Personality.create(personality, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error create a personality', err });

    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(409).send({ message: 'Personalidade já existe' });
    } else {
      const id = rows[0][0].id;
      if (images && images.length > 0) {
        images.forEach(image => {
          Personality.addImage(id, image);
        });
      }
      if (facts && facts.length > 0) {
        facts.forEach(fact => {
          Timeline.createPersonalityTimeline(fact, id);
        });
      }
      if (relations && relations.length > 0) {
        relations.forEach(r => {
          Personality.addRelation(id, r);
        });
      }
      personality.id = id;
      personality.images = images;
      personality.relations = relations;
      personality.facts = facts;
      res.send({ message: 'Personalidade Criada', personality });
    }
  });
});

router.get('/:id?', (req, res) => {
  const id = req.params.id;
  if (id) {
    Personality.get(id, (err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting personality' });
      const result = +rows[0][0].result;
      if (result === 0) {
        res.status(404).send({ message: 'Personalidade não encontrada' });
      } else {
        const personality = rows[0][0];
        Personality.getImages(personality.id, (err, rows) => {
          if (err) return res.status(500).send({ message: 'SQL error getting personality images' });
          personality.images = rows[1].map(v => v.path);
          personality.related = {};
          Personality.getRelatedP(personality.id, (err, rows) => {
            if (err) return res.status(500).send({ message: 'SQL error getting related' });
            personality.related.personalities = rows[0];
            Personality.getRelatedL(personality.id, (err, rows) => {
              if (err) return res.status(500).send({ message: 'SQL error getting related' });
              personality.related.local = rows[0];
              res.status(200).send({ personality });
            });
          });
        });
      }
    });
  } else {
    Personality.getAll((err, rows) => {
      if (err) return res.status(500).send({ message: 'SQL error getting all personalities' });

      const personalities = rows[0];
      if (personalities.length > 0) {
        res.send({ personalities: rows[0] });
      } else {
        res.status(404).send({ message: 'Sem nenhuma personalidade' });
      }
    });
  }
});

router.get('/images/:id', (req, res) => {
  const id = req.params.id;
  Personality.getImages(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error getting images' });
    res.send({ mainImage: rows[0][0], images: rows[1] });
  });
});

router.get('/related/:id', (req, res) => {
  const id = req.params.id;
  const related = {};
  Personality.getRelated(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL erro getting related', err });
    related.personalities = rows[0];
    related.locals = rows[1];
    related.routes = rows[2];
    res.send({ related });
  });
});

router.put('/:id', (req, res) => {
  const id = req.params.id;
  const personality = {
    name: req.body.name,
    job: req.body.job,
    birth: req.body.birth,
    death: req.body.death,
    desc: req.body.desc,
    profile_pic: req.body.profile_pic,
  };
  const images = req.body.images;
  const relations = req.body.relations;

  Personality.update(id, personality, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL error updating a personality', err });

    const result = +rows[0][0].result;
    if (result === 0) {
      res.status(409).send({ message: 'Personalidade não existe' });
    } else {
      const prev_image = rows[1][0].image_path;
      if (prev_image !== personality.profile_pic) {
        Image.delete(prev_image, (err, rows) => {
          if (err) return res.status(500).send({ message: 'SQL error deleting image' });
          const iPath = path.join(imagePath, prev_image);
          if (fs.existsSync(iPath)) {
            fs.unlinkSync(iPath, err => {
              if (err) return res.status(204).send({ message: 'An Error Ocurred' });
            });
          }
        });
      }
      Personality.getImages(id, (err, rows) => {
        if (err) return res.status(500).send({ message: 'SQL error getting images', err });
        if (rows[0].length > 0) {
          const images_db = rows[0].map(v => v.path);
          const toRemove = images_db.filter(i => !images.includes(i));

          if (toRemove.length > 0) {
            toRemove.forEach(i => {
              Personality.deleteImage(i);
              const iPath = path.join(imagePath, i);
              if (fs.existsSync(iPath)) {
                fs.unlinkSync(iPath, err => {
                  if (err) return res.status(204).send({ message: 'An Error Ocurred' });
                });
              }
            });
          }
        }

        if (images && images.length > 0) {
          images.forEach(image => {
            Personality.addImage(id, image);
          });
        }
        if (relations) {
          Personality.deleteRelations(id, (err, rows) => {
            if (err) return res.status(500).send({ message: 'SQL error deleting relations', err });
            if (relations.length > 0) {
              relations.forEach(r => {
                Personality.addRelation(id, r);
              });
            }
            res.send({ message: 'Personalidade Atualizada' });
          });
        } else {
          res.send({ message: 'Personalidade Atualizada' });
        }
      });
    }
  });
});

router.delete('/:id', (req, res) => {
  const id = req.params.id;
  Personality.exists(id, (err, rows) => {
    if (err) return res.status(500).send({ message: 'SQL erro verifying personality', err });

    if (+rows[0][0].result === 0) {
      return res.status(404).send({ message: 'Personalidade não encontrada' });
    } else {
      Personality.getImages(id, (err, rows) => {
        if (err) res.status(500).send({ message: 'SQL getting images', err });
        if (rows[0].length > 0) {
          const images = rows[1].map(v => v.path);
          images.forEach(image => {
            Personality.deleteImage(image);
            const currentImage = path.join(imagePath, image);
            if (fs.existsSync(currentImage)) {
              fs.unlinkSync(currentImage, err => {
                if (err) return res.status(204).send({ message: 'An Error Ocurred' });
              });
            }
          });
        }
        Personality.getTimeline(id, (err, rows) => {
          if (err) res.status(500).send({ message: 'SQL error getting timeline', err });
          const timeline = rows[0];
          timeline.forEach(time => {
            Personality.deleteTimeline(time.id);
            const currentImage = path.join(imagePath, time.image);
            if (fs.existsSync(currentImage)) {
              fs.unlinkSync(currentImage, err => {
                if (err) return res.status(204).send({ message: 'An Error Ocurred' });
              });
            }
          });

          Personality.delete(id, (err, rows) => {
            if (err) res.status(500).send({ message: 'SQL error deleting personality', err });

            const result = rows[0] != undefined ? +rows[0][0].result || rows[0][0].pic : 0;
            if (result && result === 0) {
              return res.status(404).send({ message: 'Personalidade não encontrada' });
            } else {
              const currentImage = path.join(imagePath, result);
              if (fs.existsSync(currentImage)) {
                fs.unlinkSync(currentImage, err => {
                  if (err) return res.status(204).send({ message: 'An Error Ocurred', err });
                });
              }
              return res.send({ message: 'Personalidade Eliminada' });
            }
          });
        });
      });
    }
  });
});

module.exports = { name: 'personality', router };
