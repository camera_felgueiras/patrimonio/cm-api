const db = require('../config/db');

const Image = {
  get: (path, cb) => {
    db.query('CALL get_image(?, @res)', [path], cb);
  },

  getAll: cb => {
    db.query('CALL images()', cb);
  },

  add: (path, cb) => {
    db.query('CALL add_image(?, @res)', [path], cb);
  },

  delete: (path, cb) => {
    db.query('CALL delete_image(?, @res)', [path], cb);
  }
};

module.exports = Image;
