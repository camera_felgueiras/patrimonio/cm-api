const db = require('../config/db');

const User = {
  create: (User, cb) => {
    return db.query(
      'CALL create_user(?, ?, ?, ?, @result)',
      [User.email, User.cryptPass, User.firstName, User.lastName],
      cb
    );
  },

  auth: (email, cb) => {
    return db.query('CALL auth(?, @result)', [email], cb);
  },

  getAll: cb => {
    return db.query('CALL users()', cb);
  },

  get: (id, cb) => {
    return db.query('CALL get_user(?, @result)', [id], cb);
  },

  delete: (id, cb) => {
    return db.query('CALL delete_user(?, @result)', [id], cb);
  },

  update: (user, cb) => {
    return db.query(
      'CALL update_user(?, ?, ?, ?, @res)',
      [user.id, user.email, user.firstName, user.lastName],
      cb
    );
  },

  updatePasswd: (id, password, cb) => {
    return db.query('CALL update_passwd(?, ?, @res)', [id, password], cb);
  }
};

module.exports = User;
