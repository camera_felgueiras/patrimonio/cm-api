const db = require('../config/db');

const Timeline = {
  get: cb => {
    db.query('CALL all_timeline()', cb);
  },

  getById: (id, cb) => {
    db.query('CALL get_timeline(?, @res)', [id], cb);
  },

  delete: (id, cb) => {
    db.query('CALL delete_timeline(?, @res)', [id], cb);
  },

  update: (id, fact, related, cb) => {
    const type = related.type === 'personality' ? 'p' : 'l';
    db.query(
      'CALL update_timeline(?, ?, ?, ?, ?, ?, ?, ?, @res)',
      [id, fact.id_t_related, fact.title, fact.date, fact.desc, fact.image, type, related.id],
      cb
    );
  },

  getImage: (id, cb) => {
    db.query('CALL get_timeline_image(?, @res)', [id], cb);
  },

  getRelated: (id, cb) => {
    db.query('CALL get_timeline_related(?, @res)', [id], cb);
  },

  createPersonalityTimeline: (fact, id, cb) => {
    db.query(
      'CALL relate_timeline(?, ?, ?, ?, ?, ?, @res)',
      [fact.title, fact.date, fact.desc, fact.image, 'p', id],
      cb
    );
  },

  createLocalTimeline: (fact, id, cb) => {
    db.query(
      'CALL relate_timeline(?, ?, ?, ?, ?, ?, @res)',
      [fact.title, fact.date, fact.desc, fact.image, 'l', id],
      cb
    );
  }
};

module.exports = Timeline;
