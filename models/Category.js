const db = require('../config/db');

const Category = {
  getAll: cb => {
    return db.query('CALL categories()', cb);
  },

  get: (id, cb) => {
    return db.query('CALL get_category(?, @res)', [id], cb);
  },

  getByType: (type, cb) => {
    return db.query('CALL get_categories_type(?, @res)', [type], cb);
  },

  create: (category, cb) => {
    return db.query('CALL add_category(?, ?, @result)', [category.name, category.type], cb);
  },

  delete: (id, cb) => {
    return db.query('CALL delete_category(?, @result)', [id], cb);
  }
};

module.exports = Category;
