const db = require('../config/db');

const Contacts = {
  create: (contacts, cb) => {
    db.query(
      'CALL create_contact_info(?, ?, ?, ?)',
      [contacts.phone, contacts.website, contacts.email, contacts.address],
      cb
    );
  },

  updateLocal: (id, contact, cb) => {
    db.query(
      'CALL update_local_contact(?, ?, ?, ?, ?)',
      [id, contact.phone, contact.website, contact.email, contact.address],
      cb
    );
  }
};

module.exports = Contacts;
