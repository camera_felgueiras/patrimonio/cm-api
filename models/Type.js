const db = require('../config/db');

const Type = {
  get: (id, cb) => {
    db.query('CALL get_type(?, @res)', [id], cb);
  },

  getAll: cb => {
    db.query('CALL types()', cb);
  },

  add: (type, cb) => {
    db.query('CALL add_type(?, @res)', [type.name], cb);
  },

  delete: (id, cb) => {
    db.query('CALL delete_type(?, @res)', [id], cb);
  }
};

module.exports = Type;
