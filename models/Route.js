const db = require('../config/db');

const Route = {
  get: cb => {
    db.query('CALL routes', cb);
  },

  getById: (id, cb) => {
    db.query('CALL get_route(?, @res)', [id], cb);
  },

  create: (route, cb) => {
    db.query(
      'CALL create_route(?, ?, ?, ?, ?, ?)',
      [route.title, route.desc, route.website, route.km, route.likes, route.image],
      cb,
    );
  },

  addCategory: (route_id, id, cb) => {
    db.query('CALL add_category_route(?, ?)', [route_id, id], cb);
  },

  addImage: (route_id, path, cb) => {
    db.query('CALL add_image_route(?, ?)', [route_id, path], cb);
  },

  createPath: (route_id, id_local, cb) => {
    db.query('CALL create_path(?, ?)', [route_id, id_local], cb);
  },

  addRelation: (route_id, related, cb) => {
    db.query('CALL add_r_relation(?, ?, ?)', [route_id, related.id, related.type], cb);
  },

  getCategory: (id, cb) => {
    db.query('CALL get_category_route(?)', [id], cb);
  },

  getImages: (id, cb) => {
    db.query('CALL get_route_images(?)', [id], cb);
  },

  getPath: (id, cb) => {
    db.query('CALL get_path(?)', [id], cb);
  },

  getRelated: (id, cb) => {
    db.query('CALL get_related_route(?)', [id], cb);
  },

  getRelatedL: (id, cb) => {
    db.query('CALL get_r_has_l(?)', [id], cb);
  },

  getRelatedP: (id, cb) => {
    db.query('CALL get_r_has_p(?)', [id], cb);
  },

  exists: (id, cb) => {
    db.query('CALL route_exists(?, @res)', [id], cb);
  },

  deleteImage: (path, cb) => {
    db.query('CALL delete_r_image(?)', [path], cb);
  },

  delete: (id, cb) => {
    db.query('CALL delete_route(?)', [id], cb);
  },

  deleteCategories: (id, cb) => {
    db.query('CALL delete_r_categories(?)', [id], cb);
  },

  deleteRelations: (id, cb) => {
    db.query('CALL delete_route_related(?)', [id], cb);
  },

  update: (id, route, cb) => {
    db.query(
      'CALL update_route(?, ?, ?, ?, ?, ?, @res)',
      [id, route.title, route.desc, route.website, route.km, route.image],
      cb,
    );
  },
};
module.exports = Route;
