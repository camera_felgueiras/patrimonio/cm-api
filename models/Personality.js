const db = require('../config/db');

const Personality = {
  get: (id, cb) => {
    db.query('CALL get_personality(?, @res)', [id], cb);
  },

  getAll: cb => {
    db.query('CALL personalities()', cb);
  },

  addImage: (id, path, cb) => {
    db.query('CALL personality_image(?, ?)', [id, path], cb);
  },

  getImages: (id, cb) => {
    db.query('CALL get_personality_image(?)', [id], cb);
  },

  create: (personality, cb) => {
    db.query(
      'CALL add_personality(?, ?, ?, ?, ?, ?, @res)',
      [
        personality.name,
        personality.job,
        personality.birth,
        personality.death,
        personality.desc,
        personality.profile_pic
      ],
      cb
    );
  },

  addRelation: (id, related, cb) => {
    db.query('CALL add_p_relation(?, ?, ?)', [id, related.id, related.type], cb);
  },

  getRelated: (id, cb) => {
    db.query('CALL get_related_personality(?)', [id], cb);
  },

  getRelatedP: (id, cb) => {
    db.query('CALL get_p_has_p(?)', [id], cb);
  },

  getRelatedL: (id, cb) => {
    db.query('CALL get_p_has_l(?)', [id], cb);
  },

  exists: (id, cb) => {
    db.query('CALL p_exists(?)', [id], cb);
  },

  getTimeline: (id, cb) => {
    db.query('CALL get_timeline_p(?)', [id], cb);
  },

  deleteImage: path => {
    db.query('CALL delete_p_image(?)', [path]);
  },

  deleteTimeline: id => {
    db.query('CALL delete_timeline_p(?)', [id]);
  },

  deleteRelations: (id, cb) => {
    db.query('CALL delete_personality_related(?)', [id], cb);
  },

  delete: (id, cb) => {
    db.query('CALL delete_personality(?, @res)', [id], cb);
  },

  update: (id, personality, cb) => {
    db.query(
      'CALL update_personality(?, ?, ?, ?, ?, ?, ?, @res)',
      [
        id,
        personality.name,
        personality.job,
        personality.birth,
        personality.death,
        personality.desc,
        personality.profile_pic
      ],
      cb
    );
  }
};

module.exports = Personality;
