const db = require('../config/db');

const Local = {
  get: (id, cb) => {
    db.query('CALL get_local(?, @res)', [id], cb);
  },

  getAll: cb => {
    db.query('CALL get_all_local()', cb);
  },

  getCategory: (id, cb) => {
    db.query('CALL get_category_local(?)', [id], cb);
  },

  create: (local, cb) => {
    db.query(
      'CALL create_local(?, ?, ?, ?, ?, ?, ?)',
      [
        local.title,
        local.likes,
        local.lat,
        local.lng,
        local.desc,
        local.contacts_id,
        local.mainImage
      ],
      cb
    );
  },

  addCategory: (id, category, cb) => {
    db.query('CALL add_category_local(?, ?)', [id, category], cb);
  },

  addImage: (id, image, cb) => {
    db.query('CALL add_image_local(?, ?)', [id, image], cb);
  },

  addRelation: (id, related, cb) => {
    db.query('CALL add_l_relation(?, ?, ?)', [id, related.id, related.type], cb);
  },

  getImages: (id, cb) => {
    db.query('CALL get_images_local(?)', [id], cb);
  },

  getByType: (type, cb) => {
    db.query('CALL get_local_by_type(?, @res)', [type], cb);
  },

  getRelated: (id, cb) => {
    db.query('CALL get_related_local(?)', [id], cb);
  },

  getRelatedP: (id, cb) => {
    db.query('CALL get_l_has_p(?)', [id], cb);
  },

  getRelatedL: (id, cb) => {
    db.query('CALL get_l_has_l(?)', [id], cb);
  },

  getTimeline: (id, cb) => {
    db.query('CALL get_timeline_l(?)', [id], cb);
  },

  exists: (id, cb) => {
    db.query('CALL local_exists(?, @res)', [id], cb);
  },

  deleteImage: path => {
    db.query('CALL delete_l_image(?)', [path]);
  },

  deleteTimeline: id => {
    db.query('CALL delete_timeline_l(?)', [id]);
  },

  deleteRelations: (id, cb) => {
    db.query('CALL delete_local_related(?)', [id], cb);
  },

  deleteCategories: (id, cb) => {
    db.query('CALL delete_l_categories(?)', [id], cb);
  },

  delete: (id, cb) => {
    db.query('CALL delete_local(?)', [id], cb);
  },

  update: (id, local, cb) => {
    db.query(
      'CALL update_local(?, ?, ?, ?, ?, ?, @res)',
      [id, local.title, local.lat, local.lng, local.desc, local.mainImage],
      cb
    );
  }
};

module.exports = Local;
