#!/bin/bash

printf "Mysql username: "
read user
printf "Mysql password: "
read -s pass

echo ""
read -p "Setup database [y/N]? " yn
yn=${yn:-n}

case $yn in 
  [Yy]*)
    printf "\nClearing database ... \n"
    mysql -u $user -p$pass < ./db/_tables.sql
    printf "Database OK \n"
    shopt -s nullglob dotglob
    files=(./uploads/images/*)
    if [ ${#files[@]} -gt 0 ]
    then
      printf "Cleaning images ... \n"
      rm ./uploads/images/*
      printf "Images cleared \n"
    fi 
    ;;
  *) ;;
esac


echo ""
printf "Updating procedures ... \n"
for file in ./db/procedures/*
do 
  printf "Running file ${file}\n"
  mysql -u $user -p$pass cm_pat < $file
done
